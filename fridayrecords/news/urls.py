from django.conf.urls import url

from news import views

app_name = 'news'
urlpatterns = [
    url(r'^(\d+)/$', views.show_article, name='show_newsarticle'),
    url(r'^(\d+)/([^/]+)/$', views.show_article, name='show_newsarticle'),
    url(r'^archive/$', views.show_archive, name='show_archive'),
    url(r'^$', views.main_view, name='main_view'),
]
