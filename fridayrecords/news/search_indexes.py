import datetime
from cms.models import CMSPlugin
from django.conf import settings
from django.template import RequestContext
from django.test import RequestFactory
from django.utils.encoding import force_unicode
from haystack import indexes, site
from news.models import NewsArticle
from django.utils.translation import get_language, activate
import re


def strip_tags(value):
    return re.sub(r'<[^>]*?>', ' ', force_unicode(value))


def process_plugin(placeholder, context):
    cms_plugins = CMSPlugin.objects.filter(placeholder=placeholder)
    text = ''
    if cms_plugins.count:
        for cms_plugin in cms_plugins:
            text += cms_plugin.render_plugin(context=context) + "\n"

    return strip_tags(text)


class NewsArticleIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=False)

    def index_queryset(self):
        return NewsArticle.objects.filter(published=True)

    def get_model(self):
        return NewsArticle

    def get_text(self, obj, context):
        text = ''
        text += obj.header + "\n"
        text += process_plugin(obj.anons, context) + "\n"
        text += process_plugin(obj.text, context) + "\n"

        return text

    def prepare(self, obj):
        self.prepared_data = super(NewsArticleIndex, self).prepare(obj)
        request = RequestFactory().get("/")
        request.session = {}
        activate('ru')
        self.prepared_data['text'] = self.get_text(obj, RequestContext(request))
        return self.prepared_data

site.register(NewsArticle, NewsArticleIndex)