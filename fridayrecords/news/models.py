# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.db import models
from cms.models import CMSPlugin
from cms.models.pagemodel import Page
from filer.fields.image import FilerImageField
from djangocms_text_ckeditor.fields import HTMLField


class SliderBlock(models.Model):
    title = models.CharField("Название", max_length=200)
    image = FilerImageField(null=False)
    text_big = models.TextField("Текст (крупный)")
    text_small = models.TextField("Текст (мелкий)")
    link = models.CharField("Ссылка", max_length=200, blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Блок со слайдером"
        verbose_name_plural = u"Блоки со слайдером"


class LatestNewsPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200, blank=True)

    def __unicode__(self):
        return u"%s" % self.header


class NewsArticle(models.Model):
    date = models.DateField("Дата", blank=True, db_index=True)
    header = models.CharField("Заголовок", blank=True, max_length=200)
    image = FilerImageField(null=True)
    anons = HTMLField(blank=True)
    published = models.BooleanField("Опубликовано", default=True, db_index=True)

    # def get_absolute_url(self):
    #     return reverse('show_newsarticle', args=[self.pk, ])

    def __unicode__(self):
        return "%s, %s" % (self.date.strftime("%d.%m.%Y"), self.header)

    class Meta:
        ordering = ['-date', ]

        verbose_name = u"Новость"
        verbose_name_plural = u"Новости"


