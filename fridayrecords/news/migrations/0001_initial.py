# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-15 20:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import djangocms_text_ckeditor.fields
import filer.fields.image


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0001_initial'),
        ('filer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LatestNewsPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='news_latestnewsplugin', serialize=False, to='cms.CMSPlugin')),
                ('header', models.CharField(blank=True, max_length=200, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='NewsArticle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(blank=True, db_index=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0')),
                ('header', models.CharField(blank=True, max_length=200, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba')),
                ('anons', djangocms_text_ckeditor.fields.HTMLField(blank=True)),
                ('published', models.BooleanField(db_index=True, default=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd1\x83\xd0\xb1\xd0\xbb\xd0\xb8\xd0\xba\xd0\xbe\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xbe')),
                ('image', filer.fields.image.FilerImageField(null=True, on_delete=django.db.models.deletion.CASCADE, to='filer.Image')),
            ],
            options={
                'ordering': ['-date'],
                'verbose_name': '\u041d\u043e\u0432\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u041d\u043e\u0432\u043e\u0441\u0442\u0438',
            },
        ),
        migrations.CreateModel(
            name='SliderBlock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('text_big', models.TextField(verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82 (\xd0\xba\xd1\x80\xd1\x83\xd0\xbf\xd0\xbd\xd1\x8b\xd0\xb9)')),
                ('text_small', models.TextField(verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82 (\xd0\xbc\xd0\xb5\xd0\xbb\xd0\xba\xd0\xb8\xd0\xb9)')),
                ('link', models.CharField(blank=True, max_length=200, null=True, verbose_name=b'\xd0\xa1\xd1\x81\xd1\x8b\xd0\xbb\xd0\xba\xd0\xb0')),
                ('image', filer.fields.image.FilerImageField(on_delete=django.db.models.deletion.CASCADE, to='filer.Image')),
            ],
            options={
                'verbose_name': '\u0411\u043b\u043e\u043a \u0441\u043e \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u043e\u043c',
                'verbose_name_plural': '\u0411\u043b\u043e\u043a\u0438 \u0441\u043e \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u043e\u043c',
            },
        ),
    ]
