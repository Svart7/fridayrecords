# Create your views here.
import datetime
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from models import *
from news.templatetags.news import show_articles_list


def main_view(request):
    return redirect('show_newsarticle', NewsArticle.objects.all()[:1][0].id, 'all')


def filter_articles(articles, article=None, category=0):
    if category == 'all':
        return articles

    if category:
        articles = articles.filter(categories__in=NewsCategory.objects.filter(id=category))
    elif article:
        categories = article.categories.all()
        if categories.count():
            articles = articles.filter(categories__in=categories).distinct()
        else:
            articles = articles.filter(categories__isnull=True)
    else:
        articles = articles.filter(categories__isnull=True)

    return articles


def show_article(request, id, category=None):
    article = NewsArticle.objects.get(id=id)
    today = datetime.date.today()

    articles = filter_articles(NewsArticle.objects.all(), article, category)

    return render_to_response('newsstream/article.html', {
        'article': article,
        'regions': NewsRegion.objects.all(),
        'categories': NewsCategory.objects.all(),
        'articles': articles,
        'date_widget': MonthYearWidget(blank=True).render('date', None)
    }, context_instance=RequestContext(request), )


def show_archive(request):
    article_id = request.POST.get('article', 0)

    article = NewsArticle.objects.filter(id=article_id)
    if article.count():
        article = article[0]
    else:
        article = None

    region = request.POST.get('region', 0)

    articles = NewsArticle.objects.all()
    if region and region != 'all':
        articles = articles.filter(region=region)

    category = request.POST.get('category', 0)
    articles = filter_articles(articles, article, category)

    month = int(request.POST.get('month', 0))
    year = int(request.POST.get('year', 0))

    if month and year:
        start_date = datetime.date(year, month, 1)
        month += 1
        if month > 12:
            month = 1
            year += 1

        end_date = datetime.date(year, month, 1)

        articles = articles.filter(date__range=(start_date, end_date))

    return HttpResponse(show_articles_list(request, articles))

