from django import template
from django.template.loader import render_to_string
from news.models import NewsArticle

register = template.Library()

@register.simple_tag
def last_news_article(request):
    news = NewsArticle.objects.filter(published=True)[:1]

    return render_to_string(
        'newsstream/news_articles.html',
        {
            'article': news[0]
        },
        request
    )

@register.simple_tag
def show_articles_list(request, articles):
    return render_to_string(
        'newsstream/archive_articles_list.html',
        {
            'articles': articles
        },
        request
    )