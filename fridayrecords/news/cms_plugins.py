# -*- coding: utf-8 -*-

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from news.models import NewsArticle, LatestNewsPlugin


class ActualNewsPlugin(CMSPluginBase):
    model = LatestNewsPlugin
    name = u"Блок с новостями"
    render_template = "actual.html"

    def render(self, context, instance, placeholder):
        news = NewsArticle.objects.filter(published=True)[:10]
        context['articles'] = news
        context['instance'] = instance
        return context

plugin_pool.register_plugin(ActualNewsPlugin)