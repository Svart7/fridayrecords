from django.contrib import admin
from news.models import NewsArticle, SliderBlock
from cms.admin.placeholderadmin import PlaceholderAdminMixin


class NewsArticleAdmin(PlaceholderAdminMixin, admin.ModelAdmin):
    list_display = ('date', 'header', 'published', )
    list_editable = ('published', )


class SliderAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(NewsArticle, NewsArticleAdmin)
admin.site.register(SliderBlock, SliderAdmin)