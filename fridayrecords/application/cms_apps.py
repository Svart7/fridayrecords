# -*- coding: utf-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class ApplicationApp(CMSApp):
    name = u"Заявки"

    def get_urls(self, page=None, language=None, **kwargs):
        return ["application.urls", ]

apphook_pool.register(ApplicationApp)