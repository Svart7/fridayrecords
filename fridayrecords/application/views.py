# -*- coding: utf-8 -*-
from fridayrecords import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django import forms
from django.template import RequestContext
from django.http import HttpResponse
from application.models import ApplicationEntry


class ApplicationForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        fields = ('name', 'email', 'telephone', )
        model = ApplicationEntry


def submit(request):
    return HttpResponse(application_form(request))


def application_form(request):
    success = False

    if request.method == "POST":
        form = ApplicationForm(request.POST)

        if form.is_valid():
            form.save()

            message = u'Имя: %s\nEmail: %s\nТелефон: %s\nСтраница с которой отправлена заявка: %s' % (
                form.cleaned_data['name'], form.cleaned_data['email'],
                form.cleaned_data['telephone'], request.META['HTTP_REFERER']
            )

            send_mail(u'Новая заявка',
                      message,
                      settings.MAIN_EMAIl,
                      [settings.MAIN_EMAIl],
                      fail_silently=True)
            success = True
    else:
        form = ApplicationForm()

    return render_to_string('application/form.html', {
        'form': form,
        'success': success
    }, request)