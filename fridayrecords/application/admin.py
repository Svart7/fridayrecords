from django.contrib import admin
from models import *
# from tinymce.widgets import TinyMCE


class ApplicationEntryAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'telephone', 'added', )


admin.site.register(ApplicationEntry, ApplicationEntryAdmin)


class SpeakerAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'photo_prev', 'description', 'price_1', 'price_2', 'price_3', 'weight', 'date_add')
    list_editable = ('price_1', 'price_2', 'price_3', 'weight')


admin.site.register(Speaker, SpeakerAdmin)


class SliderAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'photo_prev', 'title', ]


admin.site.register(Slider, SliderAdmin)
