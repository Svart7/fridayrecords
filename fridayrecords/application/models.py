# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from django.db import models
from cms.models import CMSPlugin, Page
import datetime
from pytils.translit import slugify, translify


class ApplicationEntry(models.Model):
    name = models.CharField("Имя", max_length=100)
    email = models.EmailField("Email", blank=True)
    telephone = models.CharField("Телефон", max_length=50, blank=True)
    added = models.DateTimeField("Дата", blank=True, auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-added', ]

        verbose_name = u"Заявки"
        verbose_name_plural = u"Заявки"

    def __unicode__(self):
        return u"Заявка %s (%s)" % (self.name, self.added.strftime("%d.%m.%Y %H:%m"),)


def file_upload_path_photo(instance, filename):
    parts = filename.rsplit('.', 1)
    s = "%s.%s" % (slugify(translify(parts[0])), slugify(translify(parts[1])))
    return "speaker_photos/%s" % s


def file_upload_path_sound(instance, filename):
    parts = filename.rsplit('.', 1)
    s = "%s.%s" % (slugify(translify(parts[0])), slugify(translify(parts[1])))
    return "speaker_sounds/%s" % s


class Speaker(models.Model):
    first_name = models.CharField(max_length=30, verbose_name=u'Имя')
    last_name = models.CharField(max_length=30, verbose_name=u'Фамилия')
    description = models.CharField(max_length=255, verbose_name=u'Подпись', help_text=u'Отображается в блоке под фамилием', blank=True)
    photo = models.ImageField(upload_to=file_upload_path_photo, verbose_name=u'Фото', max_length=255)
    example_sound = models.FileField(upload_to=file_upload_path_sound, verbose_name=u'файл озвучки', help_text=u'mp3 фрагмент', max_length=255)
    price_1 = models.DecimalField(max_digits=12, decimal_places=0, verbose_name=u'30 сек.', blank=True, null=True)
    price_2 = models.DecimalField(max_digits=12, decimal_places=0, verbose_name=u'5 мин.', blank=True, null=True)
    price_3 = models.DecimalField(max_digits=12, decimal_places=0, verbose_name=u'Озвучка А4', blank=True, null=True)
    date_add = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата добавления')
    date_mod = models.DateTimeField(auto_now=True, verbose_name=u'Дата редактирования')
    weight = models.IntegerField(default=0, verbose_name=u'Вес', help_text=u'Определяет порядок отображения элемента')

    class Meta:
        ordering = ['-weight', '-date_add']
        verbose_name = u'Диктор'
        verbose_name_plural = u'Дикторы'

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)

    def photo_prev(self):
        if self.photo:
            return u'<img src="%s" width=130>' % self.photo.url
        return ''
    photo_prev.short_description = u'Фото'
    photo_prev.allow_tags = True

def file_upload_path_photo_slider(instance, filename):
    parts = filename.rsplit('.', 1)
    s = "%s.%s" % (slugify(translify(parts[0])), slugify(translify(parts[1])))
    return "slider_photos/%s" % s

class Slider(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Заголовок')
    description = models.TextField('Описание')
    photo = models.ImageField(upload_to=file_upload_path_photo_slider, verbose_name=u'Фото', max_length=255)
    url = models.CharField(max_length=255, verbose_name=u'URL')
    weight = models.IntegerField(default=0, verbose_name=u'Вес', help_text=u'Определяет порядок отображения элемента')
    
    class Meta:
        ordering = ['-weight', '-title']
        verbose_name = u'Слайдер'
        verbose_name_plural = u'Слайдеры'

    def __unicode__(self):
        return u'%s' % (self.title)

    def photo_prev(self):
        if self.photo:
            return u'<img src="%s" width=130>' % self.photo.url
        return ''
    photo_prev.short_description = u'Фото'
    photo_prev.allow_tags = True
