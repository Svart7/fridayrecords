from django.conf.urls import url
from application import views

app_name = 'application'
urlpatterns = [
    url(r'^submit/', views.submit, name='application_submit'),
]