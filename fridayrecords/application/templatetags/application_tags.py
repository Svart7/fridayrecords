from django import template
from django.urls import NoReverseMatch
from django.urls import reverse

from application import views
from application.models import Speaker, Slider

register = template.Library()


@register.simple_tag(takes_context=True)
def application_form(context):
    try:
        reverse('application_submit')
    except NoReverseMatch:
        return ''

    return views.application_form(context.get('request'))


@register.inclusion_tag('application/speakers_block.html')
def speakers_block():
    return {
        'objects': Speaker.objects.all()
    }
    
    
@register.inclusion_tag('application/slider_block.html')
def sliders_block():
    return {
        'objects': Slider.objects.all()
    }
