# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.postgres import fields
from django.db import models


class Order(models.Model):
    name = models.CharField(u"Имя", max_length=100)
    email = models.EmailField(u"Email", blank=True)
    telephone = models.CharField(u"Телефон", max_length=50, blank=True)
    address = models.CharField(u"Адрес", max_length=1000, blank=True)
    added = models.DateTimeField(u"Дата", blank=True, auto_now_add=True, db_index=True)
    data = models.TextField(u"Данные")

    class Meta:
        ordering = ['-added', ]

        verbose_name = u"Заказ"
        verbose_name_plural = u"Заказы"

    def __unicode__(self):
        return u"Заявка #%s %s (%s)" % (self.id, self.name, self.added.strftime("%d.%m.%Y %H:%m"),)
