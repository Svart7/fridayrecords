from django.contrib import admin
from crm.models import Order


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'telephone', 'added', )


admin.site.register(Order, OrderAdmin)
