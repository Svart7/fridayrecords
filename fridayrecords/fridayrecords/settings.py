# -*- coding: utf-8 -*-

"""
Django settings for fridayrecords project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import platform

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '9!e^-7l%)qfc)cgiyaozc%k3ki*^nv4gn5269*$xym2!4a26$+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
TEMPLATE_DEBUG = False
THUMBNAIL_DEBUG = False
FILER_DEBUG = False


ALLOWED_HOSTS = ['*']


# Application definition


INSTALLED_APPS = (
    'djangocms_admin_style',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',

    'cms',
    'menus',
    'treebeard',
    'sekizai',
    'mptt',

    'filer',
    'easy_thumbnails',

    'djangocms_text_ckeditor',
    'djangocms_link',
    'djangocms_style',
    'djangocms_snippet',

    'cmsplugin_filer_file',
    'cmsplugin_filer_folder',
    'cmsplugin_filer_image',
    'cmsplugin_filer_teaser',
    'cmsplugin_filer_video',

    'image_gallery',

    'blocks',
    'audio',
    'news',
    'feedback',
    'application',
    'pricelist',
    'common',
    'crm',

    'django_assets',

    'maintenance',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    'django.middleware.locale.LocaleMiddleware',
    # 'django.middleware.doc.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',

    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
)


THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    # 'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

MAIN_EMAIl = 'info@friday-records.ru'
ROOT_URLCONF = 'fridayrecords.urls'

WSGI_APPLICATION = 'fridayrecords.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fridayrecords_db',
        'USER': 'postgres',
#        'HOST': 'localhost',
        'PASSWORD': ''
    },
}

DATABASES['master'] = DATABASES['default'].copy()

# DATABASES['master'] = DATABASES['default'].copy()
# DATABASES['master']['NAME'] = 'postgres'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'info@friday-records.ru'
EMAIL_HOST_PASSWORD = 'FRstudio2020!'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'info@friday-records.ru'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'


PROJECT_ROOT = os.path.dirname(BASE_DIR) + "/"
MEDIA_ROOT = PROJECT_ROOT + 'media/'
MEDIA_URL = '/media/'
STATIC_ROOT = MEDIA_ROOT + 'static/'
STATIC_URL = MEDIA_URL + 'static/'
UPLOAD_DIR = 'upload'

DATA_ROOT = PROJECT_ROOT + 'data/'
RUNTIME_ROOT = PROJECT_ROOT + 'runtime/'
INSTALL_ROOT = os.path.join(PROJECT_ROOT, 'install')
LOGS_ROOT = PROJECT_ROOT + 'logs/'
BACKUP_ROOT = PROJECT_ROOT + "backups/"

SITE_ID = 1

INTERNAL_IPS = ('127.0.0.1', )

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [
        os.path.join(PROJECT_ROOT, 'templates', )
    ],
    'OPTIONS': {
        'context_processors': [
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'django.template.context_processors.i18n',
            'django.template.context_processors.debug',
            'django.template.context_processors.request',
            'django.template.context_processors.media',
            'django.template.context_processors.csrf',
            'django.template.context_processors.tz',
            'sekizai.context_processors.sekizai',
            'django.template.context_processors.static',
            'cms.context_processors.cms_settings',
        ],
        'loaders': [
            ('django.template.loaders.cached.Loader', (
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            )),
        ],
    }
}]

CMS_TEMPLATES = (
    ('index.html', u'Главная',),
)

CMS_SEO_FIELDS = True

LANGUAGES = [
    ('ru', 'Russian'),
]

DJANGOCMS_STYLE_CHOICES = ['hr', 'text', 'gray_block', 'gray_arrowed', 'gray_line', ]

CMS_PAGE_MEDIA_PATH = 'cms_page_media/'

TEXT_SAVE_IMAGE_FUNCTION = 'cmsplugin_filer_image.integrations.ckeditor.create_image_plugin'

CMS_COMMON_PLUGINS = [
    'SnippetPlugin', 'TextPlugin', 'StylePlugin', 'AudioCategoryPluginBase',
    'CMSGalleryPlugin', 'OfferBlockCategoryPluginBase', 'ActualNewsPlugin',
    'IconBlockPluginBase', 'VideoBlockCategoryPluginBase',
    'FeedbackPluginBase', 'HTMLBlockPluginBase', "SoundcloudBlockPluginBase"
]

CMS_PLACEHOLDER_CONF = {
    None: {
        'plugins': CMS_COMMON_PLUGINS
    },
    'contacts': {
        'plugins': ['SnippetPlugin', ],
        'name': u"Котакты",
    },
    'top_content': {
        'plugins': ['TextPlugin', 'SalesBlockPluginBase', ],
        'name': u'Контент вверху',
    },
    'content': {
        'plugins': CMS_COMMON_PLUGINS,
        'name': u'Контент',
    },
    'footer_contacts': {
        'inherit': 'contacts',
        'name': u'Контакты в футере',
    },
    'footer_address': {
        'inherit': 'contacts',
        'name': u'Адрес в футере',
    }
}

SERIALIZATION_MODULES = {
    "json": "fridayrecords.deserializer",
}

ASSETS_DIR = STATIC_ROOT + 'built_js/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

PRODUCTION = True

__system_name = platform.system()
if __system_name in ['Darwin', 'Windows', ]:
    STATICFILES_FINDERS += (
        'common.StaticFinder',
    )

    PRODUCTION = False
    DEBUG = True
    ASSETS_DEBUG = True
    ALLOWED_HOSTS += ('127.0.0.1', )
    DATABASES['master']['PASSWORD'] = '777'
    DATABASES['default']['PASSWORD'] = '777'

    ALLOWED_HOSTS.append('192.168.1.67')
    if __system_name == 'Darwin':
        MAIN_EMAIl = 'korpiklaani@gmail.com'

if PROJECT_ROOT == '/var/www/test.friday-records.ru/':
    DATABASES['default']['NAME'] = 'fridayrecords_db_test'
    DEBUG = True

if DEBUG:
    from maintenance.debug import add_debug_panel
    add_debug_panel(globals())
    del TEMPLATES[0]["OPTIONS"]['loaders']
    TEMPLATES[0]["APP_DIRS"] = True
