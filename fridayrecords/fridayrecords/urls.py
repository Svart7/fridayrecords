# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.static import serve
from django.contrib.sitemaps.views import sitemap
from cms.sitemaps import CMSSitemap

admin.autodiscover()

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': {'cmspages': CMSSitemap}})
]

if hasattr(settings, 'DEBUG_TOOLBAR_ACTIVATED'):
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

if not settings.PRODUCTION:
    urlpatterns += static(settings.MEDIA_URL, serve, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('cms.urls'))
]
