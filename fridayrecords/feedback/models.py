# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from django.db import models
from cms.models import CMSPlugin, Page
import datetime


class FeedbackPlugin(CMSPlugin):
    def __unicode__(self):
        return u"Плагин с отзывами"

    class Meta:
        verbose_name = u"Плагин с отзывами"
        verbose_name_plural = u"Плагины с отзывами"


class FeedbackEntry(models.Model):
    name = models.CharField("Имя", max_length=100)
    email = models.EmailField("Email", blank=True)
    telephone = models.CharField("Телефон", max_length=50, blank=True)
    message = models.TextField("Сообщение", blank=True, max_length=10000)
    published = models.BooleanField("Опубликовано", default=False)
    added = models.DateTimeField("Дата", blank=True, auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-added', ]

        verbose_name = u"Отзыв"
        verbose_name_plural = u"Отзывы"

    def __unicode__(self):
        return u"Обратная связь (%s)" % (self.added.strftime("%d.%m.%Y"),)