# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.core.urlresolvers import reverse

from django.shortcuts import render_to_response
from models import FeedbackEntry
from django import forms
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect


class FeedbackForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        fields = ('name', 'email', 'telephone', 'message', )
        model = FeedbackEntry


def feedback_view(request):
    form = None

    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('%s?success=1' % reverse('feedback.views.feedback_view'))

    if not form:
        form = FeedbackForm()

    success = request.GET.get('success')

    return render_to_response('feedback/feedback.html', {
        'form': form,
        'success': success
    }, RequestContext(request), )


def submit(request):
    form = FeedbackForm(request.POST)
    success = False

    if form.is_valid():
        form.save()
        success = True

    return render_to_response('feedback/form.html', {
        'form': form,
        'success': success
    }, context_instance=RequestContext(request), )