from django.contrib import admin
from models import *
# from tinymce.widgets import TinyMCE


class FeedbackEntryAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'telephone', 'added', 'published', )
    list_editable = ('published', )


admin.site.register(FeedbackEntry, FeedbackEntryAdmin)