from django import template
from django.template.context import RequestContext
from django.template.loader import render_to_string
from feedback.models import FeedbackEntry

register = template.Library()


@register.simple_tag
def feedbacks_block(request):
    feedbacks = FeedbackEntry.objects.filter(published=True).order_by('?')[:14]

    return render_to_string('feedback/feedbacks_block.html', {
        'feedbacks': feedbacks
    }, context_instance=RequestContext(request),)