# -*- coding: utf-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class FeedbackApp(CMSApp):
    name = u"Contact us"
    urls = ["feedback.urls"]

apphook_pool.register(FeedbackApp)
