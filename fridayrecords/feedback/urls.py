from django.conf.urls import url

from feedback import views

app_name = 'feedback'
urlpatterns = [
    url(r'^$', views.feedback_view, name='feedback_view'),
    url(r'^submit/', views.submit, name='submit'),
]