# -*- coding: utf-8 -*-
from django.forms import ModelForm

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from feedback.models import FeedbackPlugin, FeedbackEntry
from feedback.views import FeedbackForm


class FeedbackPluginBase(CMSPluginBase):
    model = FeedbackPlugin

    name = u"Блок с отзывами"
    render_template = "feedback/feedbacks_block.html"

    def render(self, context, instance, placeholder):
        context['form'] = FeedbackForm()
        context['feedbacks'] = FeedbackEntry.objects.filter(published=True).order_by('?')[:14]
        return context

plugin_pool.register_plugin(FeedbackPluginBase)