from django.conf import settings
from django_assets import Bundle, register

calc_scripts = [
    'calculator/js/jquery-ui/jquery-ui.min.js',
    'calculator/js/jquery-ui/jquery.scrollTo.min.js',
    'calculator/js/jquery-ui/jquery.ui.touch-punch.min.js',
    'calculator/js/jquery-ui/sortable.js',
    'main/js/papa/papaparse.js',
    'main/js/handlebarsjs/handlebars.js',
    'calculator/js/widgets/widget.js',
    'calculator/js/widgets/button_multiselect.js',
    'calculator/js/widgets/button_select.js',
    'calculator/js/widgets/scroll_panel.js',
    'calculator/js/widgets/utils.js',
    'calculator/js/widgets/checkbox.js',
    'calculator/js/widgets/layout.js',
    'calculator/js/widgets/tooltip.js',
    'calculator/js/widgets/inputs.js',
    'calculator/js/widgets/modal.js',
    'calculator/js/widgets/fixed_panel.js',
    'calculator/js/widgets/slider.js',
    'calculator/js/widgets/price_counter.js',
    'calculator/js/calculator.js',
]

calc_scripts = Bundle(*calc_scripts, output=settings.ASSETS_DIR + 'calc.js')
register('calculator', calc_scripts)

base_scripts = [
    'maintenance/js/jquery-2.1.1.min.js',
    'main/js/jquery.herotabs.min.js',
    'main/js/main.js'
]

base_scripts = Bundle(*base_scripts, output=settings.ASSETS_DIR + 'main.js')
register('base', base_scripts)
