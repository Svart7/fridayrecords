from django.conf import settings
from django.contrib.staticfiles.finders import BaseStorageFinder
from django.core.files.storage import FileSystemStorage


class StaticFinder(BaseStorageFinder):
    storage = FileSystemStorage(settings.STATIC_ROOT, settings.STATIC_URL)
