from cms.extensions import TitleExtensionAdmin
from django.contrib import admin

from common.models import KeywordsExtension


class KeywordsExtensionAdmin(TitleExtensionAdmin):
    pass


admin.site.register(KeywordsExtension, KeywordsExtensionAdmin)