from django.conf.urls import url
from common import views

app_name = 'feedback'
urlpatterns = [
    url(r'^$', views.calculator_view, name='calculator_view'),
    url(r'^order/$', views.calculator_order, name='calculator_order'),
]