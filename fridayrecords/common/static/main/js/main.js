if ('ymaps' in window) {
    ymaps.ready(function(){
        var map = new ymaps.Map("footer_map", {
            center: [55.816292, 37.732057],
            zoom: 15,
            controls: [ ]
        }), placemark = new ymaps.Placemark([55.8187, 37.748843], {
            balloonContentHeader: "Friday Records"
        });

        map.geoObjects.add(placemark);
    });
}

function set_timer($timer, eventTime) {

    var ticks = eventTime - (new Date().getTime() / 1000),
        one_tick = function() {
            ticks--;

            if (ticks < 0) {
                ticks = 0;
            }

            var sub_ticks = ticks;
            $.each(spans, function(i, v) {
                var $block_value = $timer.find('.'+v+' .value'),
                    val = Math.floor(sub_ticks / spans_in_seconds[i]);

                sub_ticks -= val * spans_in_seconds[i];

                var zeroed_val = "0" + val;
                if (zeroed_val.length == 2) {
                    val = zeroed_val;
                }

                val = $.map(val.toString().split(''), function(symbol, i) {
                    return '<span class="symbol_'+i+'">' + symbol + '</span>';
                }).join('');

                $block_value.html(val);
            });

            if (ticks <= 0) {
                clearInterval(timer_counter);
            }
        };

	var spans = ['days', 'hours', 'minutes', 'seconds'],
		spans_in_seconds = [24*60*60, 60*60, 60, 1],
		timer_counter;

    $(function() { timer_counter = setInterval(one_tick, 1000) });
}

$(function() {
	$('.paginated_line').each(function() {
        var $plugin = $(this),
            $rows = $plugin.find('.row'),
            $news_line = $plugin.find('.line'),
            $wrapper = $plugin.find('.wrapper');

        if ($rows.length < 2) {
            return;
        }

        var $paginator = $('<div class="paginator"></div>');
        if ($wrapper.length) {
            $paginator.insertAfter($wrapper);
        } else {
            $plugin.append($paginator);
        }

        $.each($rows, function(i) {
            var $link = $('<a href="#" class="page">'+(i+1)+'</a>').appendTo($paginator).click(function() {
                if ($link.hasClass('active')) {
                    return false;
                }

                $paginator.find('.active').removeClass('active');
                $link.addClass('active');

                $news_line.animate({
                    'margin-left': -1 * i * $rows.width()
                });

                return false;
            });

            if (i == 0) {
                $link.addClass('active');
            }
        });
    });



    $('.ajax_form').each(function(){
        var $form = $(this),
            action = $form.attr('action'),
            onFormSubmit = function() {
                if (action) {
                    $.post($form.attr('action'), $form.serialize(), function(html) {
                        var $new_form = $(html);
                        $form.replaceWith($new_form);
                        $new_form.show().submit(onFormSubmit);
                        $form = $new_form;
                    }, 'html');

                    $form.html('<p style="text-align: center; padding: 5px;"><img src="/media/img/preloader.gif"></p>');
                }

                return false;
            };

        $form.submit(onFormSubmit);
    });
});


$("a[href*=#]:not([href=#])").bind('click', function (event) {
  var thisHash = this.hash;
  var targetOffset = $(thisHash).offset().top;
  $("html,body").stop().animate({
    scrollTop: targetOffset - 100
  }, 500 );
  location.hash = thisHash;

});

(function() {
  if (!$('.pluso').length) {
      return;

  }

  if (window.pluso) if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();

$('.tabs-vert').herotabs({
    duration: 500,
    delay: 5000
});