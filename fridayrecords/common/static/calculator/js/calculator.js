$(function () {
    var CALCULATOR_SELECTOR = '#calculator',
        INSTRUMENT_VOCAL = "Вокал",
        INSTRUMENT_DRUMS = "Барабаны",
        INSTRUMENTS_CLASSES = ["instruments_selector", "solo_instruments_selector"],
        JSCSVDATA = {},
        SERVICE_IDS = {},
        $calculator = $(CALCULATOR_SELECTOR),
        layout_widget,
        global_input_params = {},
        service_input_params = {};

    window.yaFlags = {load: false, client: false, service: false, calc: false, order: false, buy: false};

    if (!yaFlags["load"]){
        yaFlags["load"] = true;
        if (window.yaCounter23926786) window.yaCounter23926786.reachGoal('load');
    }

    WIDGETS.set_widget_option('Checkbox', "default_options_selector", {
        ".gift_certificate": {
            "rebuildLayout": function (widget) {
                var $block = widget.$element.parents('.block_3d'),
                    delivery_widget = $block.find('.delivery').data('widget');

                if (widget.selected) {
                    delivery_widget.showParent();
                } else {
                    delivery_widget.hideParent();
                }

            }
        },
        ".need_mixing": {
            "rebuildLayout": function (widget) {
                var $mixing_mode_wrapper = widget.$element.parents('.block_3d').find('.mixing_mode_wrapper'),
                    $checkboxes = $mixing_mode_wrapper.find('.checkbox');

                if (widget.selected) {
                    $mixing_mode_wrapper.show();
                } else {
                    $mixing_mode_wrapper.hide();
                }

                $.each($checkboxes, function () {
                    if (widget.selected) {
                        $(this).data('widget').showParent();
                    } else {
                        $(this).data('widget').hideParent();
                    }
                });
            }
        },
        ".music_writing": {
            "rebuildLayout": function (widget) {
                var $block = widget.$element.parents('.block_3d'),
                    $music_options = $block.find('.music_options');

                if (widget.selected) {
                    $music_options.show();
                } else {
                    $music_options.hide();
                }
            }
        },
        ".vocal_record": {
            rebuildLayout: function (widget) {
                var $block = widget.$element.parents('.block_3d'),
                    need_tuning_widget = $block.find('.need_tuning').data('widget');

                if (widget.selected) {
                    need_tuning_widget.showParent();
                } else {
                    need_tuning_widget.hideParent();
                }
            }
        }
    });

    WIDGETS.set_widget_option('ButtonSelect', "default_options_selector", {
        ".step1_selector": {
            "onClick": function () {
                if (!yaFlags["client"]){
                    yaFlags["client"] = true;
                    if (window.yaCounter23926786) window.yaCounter23926786.reachGoal('client');
                }
                layout_widget.goto_panel('.step2');
            },
            "titles": function() {
                return $.grep(JSCSVDATA['client_service'].meta.fields, function (name) {
                    return name && name != 'id' && name != 'description';
                });
            },
            "onChanging": function (widget, client) {
                global_input_params["client"] = client;
            }
        },
        ".step2_selector": {
            "titles": function() {
                var client = global_input_params["client"];
                if (!client) {
                    return;
                }

                var titles = [];

                $.each(JSCSVDATA['client_service'].data, function () {
                    var row = this;
                    if (row[client]) {
                        titles.push({
                            'id': row["id"],
                            'title': row[""],
                            'description': row["description"]
                        });
                    }
                });

                return titles;
            },
            "onClick": function (widget, $button) {
                if (!yaFlags["service"]){
                    yaFlags["service"] = true;
                    if (window.yaCounter23926786) window.yaCounter23926786.reachGoal('service');
                }

                if ($button.tooltip("instance")) {
                    $button.tooltip('close');
                }
            },
            "onChanging": function (widget, service) {
                global_input_params["service"] = service;
                layout_widget.open_service(service);
            }
        },
        ".step1-arangement .arangement_type": {
            "onChange": function(widget, new_value) {
                layout_widget.open_service("arangement;" + new_value + ";arangement-full");
            }
        }
    });

    var urgency_titles = function () {
        var titles = [];

        var metadata_dict = {};
        $.each(JSCSVDATA['metadata'].data, function () {
            metadata_dict[this["label"]] = float_parse(this["value"]);
        });

        $.each(JSCSVDATA['urgency_levels'].data, function () {
            var row = this;
            if (row["Услуга"] == SERVICE_IDS[global_input_params["service"]]) {
                var make_title = function (letter) {
                    return {
                        percent: metadata_dict["Urgency " + letter],
                        days: row[letter],
                        urgency: letter,
                        title: 'В течении ' + row[letter] + ' дней'
                    }
                };

                titles = [make_title('C'), make_title('B'), make_title('A')];
            }
        });

        titles.push({
            percent: metadata_dict["Urgency 1"],
            days: 0,
            urgency: '1',
            title: 'В день обращения'
        });

        return titles;
    };

    WIDGETS.set_widget_option('Modal', "default_options_selector", {
       ".order_block": {
           onShow: function (widget) {
               var input_selectors = ['#order_name', '#order_phone', '#order_email'],
                   order_params = {},
                   $order_p = widget.$element.find('.order_p'),
                   $order_form = widget.$element.find('.order_form'),
                   is_loading = false,
                   send_order = function () {
                       if ($order_p.is('.disabled')) {
                           return false;
                       }
                       is_loading = true;
                       $order_p.addClass('disabled');

                       var params = $.extend({}, $calculator.find('.price_counter').data('widget').getParams());

                       $.extend(params, global_input_params);

                       params['service'] = SERVICE_IDS[params['service']];

                       $.extend(params, service_input_params);
                       
                       if (service_input_params['urgency']) {
                           urgency_titles().forEach(function (title) {
                               if (title.urgency == service_input_params['urgency']) {
                                   params['urgency_text'] = title.title;
                                   params['urgency_percent'] = title.percent;
                                   return false;
                               }
                           })
                       }

                       $.extend(params, order_params);
                       $.post($order_form.attr('action'), {
                           "params": JSON.stringify(params),
                           "csrfmiddlewaretoken": widget.$element.find('[name="csrfmiddlewaretoken"]').val()
                       }, function (data) {
                            if (data['order_id']) {
                                $order_p.removeClass('disabled');
                                widget.$element.find('.order_number').html(data['order_id']);
                                widget.$element.addClass('success');
                            }

                            is_loading = false;
                       }, 'json');

                       if (!yaFlags["buy"]){
                            yaFlags["buy"] = true;
                            if (window.yaCounter23926786) window.yaCounter23926786.reachGoal('buy');
                       }

                       return false;
                   };

               if (service_input_params['delivery']) {
                   widget.$element.find('.address_wrapper').removeClass('hidden');
                   input_selectors.push("#order_address");
               } else {
                   widget.$element.find('.address_wrapper').addClass('hidden');
               }

               widget.$element.find('.order_button').click(send_order);
               $order_form.submit(send_order);

               widget.$element.removeClass('success');

               $(input_selectors.join(',')).on('change keyup', function () {
                   var empty_values = false;

                   $.each(input_selectors, function () {
                       var selector = this.toString(),
                           $input = $(selector),
                           key = selector.replace('#', "");

                       order_params[key] = $input.val();

                       if (!order_params[key]) {
                           empty_values = true;
                       }
                   });

                   if (is_loading) {
                       return;
                   }

                   if (empty_values) {
                       $order_p.addClass('disabled');
                   } else {
                       $order_p.removeClass('disabled');
                   }
               });
           }
       }
    });

    WIDGETS.set_widget_option('Slider', "default_options_selector", {
        ".urgency": {
            "titles": urgency_titles
        },
        ".vocal_level": {
            onInit: function (widget) {
                var client = global_input_params["client"];
                if (client == "Новичок" || client == "Прочее") {
                   widget.setValue("Средне");
                } else if (client == "Вокалист") {
                   widget.setValue("Хорошо");
                }
            }
        }
    });

    WIDGETS.set_widget_option('PriceCounter', "default_options", {
        onClickOrder: function () {
            var $order_block = $calculator.find('.order_block');

            if (!yaFlags["order"]){
                yaFlags["order"] = true;
                if (window.yaCounter23926786) window.yaCounter23926786.reachGoal('order');
            }

            $order_block.data('widget').show();
        }
    });

    WIDGETS.set_widget_option('ButtonMultiselect', "default_options", {
        "titles": function(widget) {
            var skip_vocal = widget.$element.hasClass('live_arangement_instruments'),
                titles = [];

            $.each(JSCSVDATA['instrument_data'].data, function () {
                var row = this,
                    id = row['id'];

                if (skip_vocal && id == "vocal") {
                    return true;
                }

                titles.push({
                    'id': row["id"],
                    'title': row["instrument"]
                });
            });

            return titles;
        }
    });

    layout_widget = WIDGETS["Layout"]($calculator);
    layout_widget.set_on_panel_open('.step2', function ($step2) {
        var service_selector = $step2.find('.step2_selector').data('widget');
        service_selector.build();

        if (!service_selector.getValue()) {
            layout_widget.close_service_panels();
        }
    });

    var panel_check_bounds = null;
    layout_widget.set_on_panel_open('.price_panel', function ($price_panel) {
        var fixed_panel = $price_panel.find('.price_panel_fixed').data('widget'),
            panel_check_bounds = fixed_panel.check_bounds.bind(fixed_panel),
            $min_bound_panel = layout_widget.price_panel_min();

        if ($min_bound_panel) {
            fixed_panel.min_bound = $min_bound_panel.offset().top;
        } else {
            var $step2_panel = $calculator.find('.step2');
            fixed_panel.min_bound = $step2_panel.offset().top + $step2_panel.outerHeight(true);
        }

        fixed_panel.init_height();

        layout_widget.add_on_scroll(panel_check_bounds);
    });

    layout_widget.set_on_panel_close('.price_panel', function () {
        layout_widget.remove_on_scroll(panel_check_bounds);
    });

    layout_widget.set_on_service_open(function (service_id, $panels) {
        service_input_params = {};

        var price_widget = $panels.find('.price_counter').data('widget'),
            count_price = function () {
                var params = $.extend({ JSCSVDATA: JSCSVDATA }, global_input_params);
                $.extend(params, service_input_params);

                params["service"] = SERVICE_IDS[params["service"]];

                price_widget.set_price(params);
            },
            instruments_widgets = [],
            process_instruments = function () {
                if (service_id.indexOf('arangement') != -1) {
                    return;
                }

                var instruments = instruments_widgets.map(function(widget) { return widget.getValue(); }).join(";"),
                    $need_drum_correction = $panels.find('.need_drum_correction'),
                    need_tuning = $panels.find('.need_tuning').data('widget');

                if ($need_drum_correction.length) {
                    if (instruments.indexOf(INSTRUMENT_DRUMS) == -1) {
                        $need_drum_correction.data('widget').hideParent();
                    } else {
                        $need_drum_correction.data('widget').showParent();
                    }
                }

                if (instruments.indexOf(INSTRUMENT_VOCAL) == -1) {
                    need_tuning.hideParent();
                } else {
                    need_tuning.showParent();
                }
            };

        console.group();
        console.log('opening panels ', $panels);

        var $widgets = $panels.find('.price_tracking'),
            $urgency = $panels.find('.urgency'),
            select_example_panels = function () {
                $panels.each(function () {
                    var $panel = $(this);
                    if ($panel.hasClass('example')) {
                        if (service_input_params['mixing_mode'] == $panel.data('mixing-mode')) {
                            $panel.show();
                        } else {
                            $panel.hide();
                        }
                    }
                })
            };

        $widgets.each(function () {
            var $widget = $(this),
                param_id = $widget.data('name');

            if (!param_id) {
                param_id = $widget.attr('name');
            }

            if (!param_id) {
                console.error('cannot find param name for ', $widget);
                return true;
            }

            console.log('start tracking ', $widget);

            var widget = $widget.data('widget');
            if (!widget) {
                console.error('widget for ', param_id, "not found");
                return true;
            }

            var reset_skipped = false;
            if ($widget.hasClass('skip_reset') && service_id != "arangement") {
                reset_skipped = true;
                service_input_params[param_id] = widget.getValue();
            }

            if (!reset_skipped) {
                var default_value = $widget.data('default-value');
                if (default_value !== undefined) {
                    widget.setValue(default_value);
                } else {
                    default_value = widget.getValue();
                    $widget.data('default-value', default_value);
                }

                service_input_params[param_id] = default_value;
            }

            widget.options.onChanging = function (widget, new_value) {
                service_input_params[param_id] = new_value;
                count_price();
            };

            if (INSTRUMENTS_CLASSES.indexOf(param_id) != -1) {
                widget.options.onChange = function () {
                    process_instruments();
                }
            }

            if (param_id == 'mixing_mode') {
                widget.options.onChange = select_example_panels;
            }

            if (INSTRUMENTS_CLASSES.indexOf(param_id) != -1) {
                instruments_widgets.push(widget);
            }
        });

        $widgets.each(function () {
            var widget = $(this).data('widget');
            if ('rebuildLayout' in widget.options) {
                widget.options.rebuildLayout(widget);
            }
        });


        if (instruments_widgets.length > 0) {
            process_instruments();
        }

        console.groupEnd();

        console.log("on service open", service_id, service_input_params);
        count_price();

        if ($urgency.length) {
            $urgency.data('widget').build_titles();
        }

        var $vocal_level = $panels.find('.vocal_level');
        if ($vocal_level.length) {
            var widget = $vocal_level.data('widget');
            widget.options.onInit(widget);
        }

        select_example_panels();
    });

    layout_widget.$element.find('.js_csv_data').each(function () {
        var $this = $(this);
        JSCSVDATA[$this.data('id')] = $this.data('widget').data;
    });

    $.each(JSCSVDATA['client_service'].data, function () {
        var row = this;
        SERVICE_IDS[row['id']] = row[""];
    });

    layout_widget.open_panel('.step1');
});