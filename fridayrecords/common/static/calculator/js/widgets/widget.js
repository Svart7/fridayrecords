var WIDGETS = {
    '_widget_options': {

    },
    'set_widget_option': function(widget_name, option_name, value) {
        if (widget_name in WIDGETS._widget_options) {
            WIDGETS._widget_options[widget_name][option_name] = value;
        }
    },
    'init_widget': function (widget_name, properties, widget_options) {
        if (!widget_options) {
            widget_options = {};
        }
        WIDGETS._widget_options[widget_name] = widget_options;

        WIDGETS[widget_name] = function ($element, options) {
            var widget_options = WIDGETS._widget_options[widget_name];

            if (!options) {
                options = {};
            }

            if ("default_options" in widget_options) {
                $.extend(true, options, widget_options["default_options"]);
            }

            if ("default_options_selector" in widget_options) {
                $.each(widget_options["default_options_selector"], function (selector, options_data) {
                    if ($element.is(selector)) {
                        $.extend(true, options, options_data);
                    }
                });
            }

            var widget = {
                "options": options,
                "$element": $element,
                "_use_inner_template": function () {
                    var $template = $element.children('.template');
                    if ($template.length) {
                        var template_widget = WIDGETS['JSTemplateData']($template);
                        widget.template = template_widget.template;
                    }
                }
            };

            $.each(properties, function (key, value) {
                widget[key] = value;
            });

            if (!('template' in widget)) {
                widget._use_inner_template();
            }

            $element.data('widget', widget);

            if ('init' in widget) {
                widget.init($element, options);
            }

            return widget;
        };
    }
};