var widget_default = {
    events: 'change keyup',
    init: function () {
        var widget = this;

        widget.$element.on(widget.events, function () {
            if ('onChanging' in widget.options) {
                widget.options.onChanging(widget, widget.getValue());
            }

            if ('onChange' in widget.options) {
                widget.options.onChange(widget, widget.getValue());
            }
        });
    },
    getValue: function () {
        return this.$element.val();
    },
    setValue: function (value) {
        this.$element.val(value);
    }
};

var select_widget = $.extend({}, widget_default);
select_widget.events = 'change';
WIDGETS.init_widget('Select', select_widget);

var integer_widget = $.extend({}, widget_default);
integer_widget.events = "change keyup mousewheel";
WIDGETS.init_widget('InputInteger', integer_widget);

WIDGETS.init_widget('Textarea', $.extend({}, widget_default));