WIDGETS.init_widget('Modal', {
    init: function () {
        var widget = this;

        widget.$element.hide();
        widget.$parent = widget.$element.parent();
    },
    
    show: function () {
        var widget = this;

        widget.$overlay = $('<div class="overlay"></div>').appendTo('body');
        widget.$overlay.click(function () {
            widget.hide();
        });

        widget.$element.appendTo('body');
        widget.$element.show();

        if (widget.options['onShow']) {
            widget.options['onShow'](widget);
        }
    },
    
    hide: function () {
        var widget = this;

        if (widget.$overlay) {
            widget.$overlay.remove();
            widget.$overlay = null;
        }

        widget.$element.appendTo(widget.$parent);
        widget.$element.hide();
    }
});