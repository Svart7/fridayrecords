var HEADER_TITLE_LEVEL = 'Параметр №1 (уровень исполнения)',
    HEADER_TITLE_INSTRUMENT = 'Параметр №2 (инструмент)',
    HEADER_TITLE_MIXING_MODE = 'Параметр №3 (вид сведения)',
    HEADER_TITLE_TYPE = 'Параметр №4 (вид аранжировки)',
    HEADER_TITLE_SERVICE = 'Услуга',
    HEADER_TITLE_WORK = 'Работа',
    HEADER_TITLE_ANALYTICS1 = 'Аналитика №1',
    HEADER_TITLE_ANALYTICS2 = 'Аналитика №2',
    HEADER_TITLE_DEPENDS_SONGS_COUNTER = 'ЗависитОтКоличестваПесен?',
    HEADER_TITLE_WORK_HOURS = 'Часов запись',
    HEADER_TITLE_WORK_PRICE = 'Подрядчикам',

    SERVICE_RECORD = 'Запись песни под минус',
    SERVICE_MULTITRACK = 'Многоканальная запись',
    SERVICE_LIVE = 'Студийная запись (Live)',
    SERVICE_ARANGEMENT = 'Аранжировка',
    SERVICE_LIVE_ARANGEMENT = "Живая аранжировка",
    SERVICE_ELECTRO_ARANGEMENT = "Электронная аранжировка",
    SERVICE_TEXT = "Написание текста",
    SERVICE_MIXING = "Сведение и мастеринг",
    SERVICE_RENT = "Аренда",
    SERVICE_WELLDONE = "Песня \"под ключ\"",
    SERVICE_PRODUCE = "Создание минусовки",
    SERVICE_OTHER = "Прочее",

    SERVICE_WORK_ADDITIONAL_VOCAL_RECORD = "Запись дополнительного вокала",
    SERVICE_WORK_ADDITIONAL_VOCAL_TUNE = "Тюн дополнительного вокала",
    SERVICE_WORK_ONE_TRACK_MIXING = "Сведение (одной дорожки)",
    SERVICE_WORK_ONE_INSTRUMENT_MIXING = "Сведение (одного инструмента)",
    SERVICE_WORK_ONE_INSTRUMENT_RECORD = "Запись (одного инструмента)",
    SERVICE_WORK_PLUGING_INSTRUMENT = "Подключение инструментов (один инструмент)",
    SERVICE_WORK_WRITE_MUSIC = "Написание музыки",
    SERVICE_WORK_WRITE_TEXT = "Написание текста",
    SERVICE_WORK_RECORD_VOCAL = "Запись вокала",
    SERVICE_WORK_MASTERING_STANDALONE = "Мастеринг (отдельный)",
    SERVICE_WORK_MASTERING = "Мастеринг",

    SERVICE_WORK_ADDITIONAL_INSTRUMENT = "Запись дополнительного инструмента",

    MIXING_PARAMS = {
        "Мастеринг": 'need_mastering',
        "Сведение": 'need_mixing'
    },

    ARRANGEMENT_TYPES = {
        "electro-arangement": SERVICE_ELECTRO_ARANGEMENT,
        "live-arangement": SERVICE_LIVE_ARANGEMENT
    };

WIDGETS.init_widget('PriceCounter', {
    "init": function () {
        var widget = this;

        widget.$actual_price = widget.$element.find('.actual_price_display');
        widget.$original_price = widget.$element.find('.original_price');
        widget.$discount_price = widget.$element.find('.discount_price_display');
        widget.$discount = widget.$element.find('.discount_display');
        widget.$order_button = widget.$element.find('.order_button');

        widget._saved_params = {};

        widget.$order_button.click(function () {
            if (widget.$element.hasClass('disabled')) {
                return false;
            }

            if (widget.options['onClickOrder']) {
                widget.options['onClickOrder']();
            }

            return false;
        })
    },
    "no_price": function () {
        this._saved_params = {};
        this.$element.addClass('disabled');
    },
    "get_price": function () {
        return this.$actual_price.data('widget').getValue();
    },
    "set_price": function (params) {
        var widget = this,
            one_song_time = 0,
            one_song_price = 0,
            rec_time = 0,
            contractor_price = 0,
            mix_time = 0,
            tracks = 0,
            service = params['service'],
            songs_counter = params["songs_counter"],
            instruments_param = params['instruments_selector'],
            solo_instruments = [],
            live_instruments = [];

        if (!widget.metadata_dict) {
            widget.metadata_dict = {};
            $.each(params['JSCSVDATA']['metadata'].data, function () {
                widget.metadata_dict[this["label"]] = float_parse(this["value"]);
            });
        }

        if (params['tracks_counter']) {
            tracks = parseInt(params['tracks_counter']);
            if (tracks < 1) {
                tracks = 1;
            }
        }

        console.log('input params', params);

        if (service == SERVICE_ARANGEMENT) {
            if (!params['arangement_type']) {
                widget.no_price();
                return;
            } else {
                params['need_mixing'] = true;
                service = ARRANGEMENT_TYPES[params['arangement_type']];
            }
        } else if (service == SERVICE_WELLDONE) {
            if (
                (!params['arangement_type'] && params["music_writing"]) ||
                (!params["music_writing"] && !params["vocal_record"] && !params["text_writing"])
            ) {
                widget.no_price();
                return;
            }
        }

        if ([SERVICE_MULTITRACK, SERVICE_LIVE, SERVICE_MIXING, SERVICE_LIVE_ARANGEMENT].indexOf(service) != -1) {
            params['mixing_mode'] = 'Стандарт';
        }


        if ([SERVICE_MULTITRACK, SERVICE_LIVE, SERVICE_LIVE_ARANGEMENT].indexOf(service) != -1) {
            if (!instruments_param && service != SERVICE_LIVE_ARANGEMENT) {
                widget.no_price();
                return;
            }

            if (instruments_param) {
                solo_instruments = instruments_param.split(";");

                if (service == SERVICE_LIVE) {
                    live_instruments = solo_instruments;
                    solo_instruments = params['solo_instruments_selector'];
                    if (solo_instruments) {
                        solo_instruments = solo_instruments.split(";");
                    } else {
                        solo_instruments = [];
                    }
                }

                $.each(params['JSCSVDATA']['instrument_data'].data, function () {
                    var row = this,
                        needed_instrument = function (i) {
                            return i == row["instrument"];
                        },
                        found_solo = solo_instruments.filter(needed_instrument),
                        found_live = live_instruments.filter(needed_instrument);

                    tracks += parseInt(row["solo"]) * found_solo.length;
                    tracks += parseInt(row["live"]) * found_live.length;
                });
            }
        }


        console.group();

        $.each(params['JSCSVDATA']['labor_costs'].data, function () {
            var row = this;
            if (row[HEADER_TITLE_SERVICE] == service) {
                var time = float_parse(row[HEADER_TITLE_WORK_HOURS]),
                    price = float_parse(row[HEADER_TITLE_WORK_PRICE]),
                    work = row[HEADER_TITLE_WORK],
                    time_k = 1;

                if (
                    row[HEADER_TITLE_LEVEL] != params['play_level'] &&
                    row[HEADER_TITLE_LEVEL] != ""
                ) {
                    return true;
                }

                var instrument = row[HEADER_TITLE_INSTRUMENT];
                if (instrument != "") {
                    var occurences = solo_instruments.filter(function (i) { return i == instrument }).length;
                    if (occurences == 0) {
                        return true;
                    }

                    time_k *= occurences;
                }

                if (params['mixing_mode']) {
                    var mixing_mode = row[HEADER_TITLE_MIXING_MODE];
                    if (
                        mixing_mode != "" &&
                        mixing_mode != params['mixing_mode']
                    ) {
                        return true;
                    }
                }

                console.log(params["need_tuning"]);
                console.log(row[HEADER_TITLE_ANALYTICS1]);
                console.log(service);
                console.log(work);

                if (!params["need_tuning"] &&
                    row[HEADER_TITLE_ANALYTICS1] == "Тюн") {
                    return true;
                }

                if (!params["need_mixing"] &&
                    row[HEADER_TITLE_ANALYTICS1] == "Звук") {
                    return true;
                }

                if (!params['need_drum_correction'] &&
                    row[HEADER_TITLE_ANALYTICS1] == "Выравнивание барабанов") {
                    return true;
                }

                if (service == SERVICE_RECORD) {
                    if (work == SERVICE_WORK_ADDITIONAL_VOCAL_RECORD) {
                        time_k *= params['singers_counter'] - 1;
                    } else if (work == SERVICE_WORK_ADDITIONAL_VOCAL_TUNE) {
                        time_k *= params['singers_counter'] - 1;
                    } else if (work == SERVICE_WORK_ONE_TRACK_MIXING) {
                        time_k *= params['singers_counter'];
                    }
                } else if (service == SERVICE_MULTITRACK) {
                    if (work == SERVICE_WORK_ADDITIONAL_INSTRUMENT) {
                        // time_k *= params['additional_instruments'];
                        return true;
                    }
                } else if (service == SERVICE_LIVE) {
                    if (work == SERVICE_WORK_PLUGING_INSTRUMENT) {
                        time_k *= live_instruments.length;
                    }
                } else if (service == SERVICE_TEXT) {
                    if (row[HEADER_TITLE_TYPE] && row[HEADER_TITLE_TYPE] != params["text_type"]) {
                        return true;
                    }
                } else if (service == SERVICE_MIXING) {
                    var work_type = row[HEADER_TITLE_TYPE],
                        param_name = MIXING_PARAMS[work_type];

                    if (!params[param_name]) {
                        return true;
                    }

                    var mastering_standalone = (params["need_mastering"] && !params["need_mixing"]);

                    if (work == SERVICE_WORK_MASTERING_STANDALONE) {
                        if (!mastering_standalone) {
                            return true;
                        }
                    } else if (work == SERVICE_WORK_MASTERING) {
                        if (mastering_standalone) {
                            return true;
                        }
                    } else if (work == SERVICE_WORK_ONE_TRACK_MIXING) {
                        time_k *= tracks;
                    }
                } else if (service == SERVICE_RENT) {
                    time_k *= parseInt(params['rent_hours']);
                } else if (service == SERVICE_WELLDONE) {
                    if (work == SERVICE_WORK_WRITE_MUSIC) {
                        if (!params["music_writing"]) {
                            return true;
                        }

                        if (row[HEADER_TITLE_TYPE] != ARRANGEMENT_TYPES[params["arangement_type"]]) {
                            return true;
                        }
                    } else if (work == SERVICE_WORK_RECORD_VOCAL) {
                        if (!params["vocal_record"]) {
                            return true;
                        }
                    } else if (work == SERVICE_WORK_WRITE_TEXT) {
                        if (!params["text_writing"]) {
                            return true;
                        }
                    }
                } else if (service == SERVICE_PRODUCE) {
                    var music_type = row[HEADER_TITLE_TYPE];
                    if (music_type != params['music_type']) {
                        return true;
                    }
                }

                if ([SERVICE_MULTITRACK, SERVICE_LIVE, SERVICE_LIVE_ARANGEMENT].indexOf(service) != -1) {
                    if (row[HEADER_TITLE_WORK] == SERVICE_WORK_ONE_INSTRUMENT_MIXING) {
                        time_k *= tracks;
                    }
                    if (row[HEADER_TITLE_WORK] == SERVICE_WORK_ONE_INSTRUMENT_RECORD) {
                        time_k *= tracks;
                    }
                }

                one_song_time += (time * time_k);
                one_song_price += price;

                if (row[HEADER_TITLE_DEPENDS_SONGS_COUNTER] == "Да") {
                    time_k *= songs_counter;
                    price *= songs_counter;
                }

                if (time) {
                    console.log("using row", row, "time", time, "*", time_k);
                } else {
                    console.log("using row", row, "price", price);
                }

                contractor_price += price;

                time *= time_k;
                if (row[HEADER_TITLE_ANALYTICS2] == "Запись") {
                    rec_time += time;
                } else if (row[HEADER_TITLE_ANALYTICS2] == "Сведение") {
                    mix_time += time;
                }
            }
        });

        console.groupEnd();


        var total_time = rec_time + mix_time,
            urgency_multiplication = 1.0,
            price_addition = 0,
            base_price = parseInt(total_time * widget.metadata_dict["Стоимость часа"]),
            price_no_discounts = parseInt(one_song_time  * widget.metadata_dict["Стоимость часа"]);

        if (songs_counter) {
             price_no_discounts *= songs_counter;
        }


        if (params['urgency']) {
            urgency_multiplication += widget.metadata_dict["Urgency " + params["urgency"]] / 100.0;
        }

        var employee_price = parseInt(total_time  * urgency_multiplication * widget.metadata_dict["Ставка звукорежиссера"])

        if (contractor_price) {
            var price_multiplier = widget.metadata_dict["Наценка"];

            base_price += contractor_price * price_multiplier;
            price_no_discounts += songs_counter * one_song_price * price_multiplier;
        }

        if (params["gift_certificate"]) {
            price_addition = widget.metadata_dict['Подарочный сертификат'];

            if (params['delivery']) {
                price_addition += widget.metadata_dict['Доставка'];
            }
        }

        var add_to_price = function (price) {
            return (price * urgency_multiplication) + price_addition;
        };

        base_price = add_to_price(base_price);
        price_no_discounts = add_to_price(price_no_discounts);

        var discount_percent = 0;
        if (params["vip"]) {
            discount_percent = widget.metadata_dict['Скидка (карточка)'];
        }

        if (params["songs_counter"]) {
            discount_percent += (params["songs_counter"] - 1) *  widget.metadata_dict['Скидка (за доп. песню)'];
        }

        var max_discount_percent = widget.metadata_dict["Скидка MAX"];
        if (discount_percent > max_discount_percent) {
            discount_percent = max_discount_percent;
        }

        base_price -= base_price * discount_percent / 100.0;

        var discount = price_no_discounts - base_price;

        var tax_price = base_price * 0.07,
            manager_price = base_price * 0.07,
            profit_price = base_price - tax_price - manager_price - contractor_price - employee_price;

        if (discount > 0) {
            widget.$discount.show();
            widget.$discount_price.data('widget').setValue(discount);
            widget.$original_price.data('widget').setValue(price_no_discounts);
        } else {
            widget.$discount.hide();
        }

        widget.$element.removeClass('disabled');

        var $actual_price_wrapper = widget.$element.find('.actual_price');
        $actual_price_wrapper.removeClass('disabled');

        widget.$actual_price.data('widget').setValue(base_price);

        if (base_price == 0) {
            if (service == SERVICE_OTHER) {
                $actual_price_wrapper.addClass('disabled');
            } else {
                widget.no_price();
            }
        } else {
            if (!yaFlags["calc"]){
                yaFlags["calc"] = true;
                if (window.yaCounter23926786) window.yaCounter23926786.reachGoal('calc');
            }
        }

        if (!instruments_param && service == SERVICE_LIVE_ARANGEMENT) {
            widget.$order_button.parent().addClass('disabled');
        } else {
            widget.$order_button.parent().removeClass('disabled');

            widget._saved_params = {
                record_time: rec_time,
                mixing_time: mix_time,
                base_price: base_price,
                discount: discount,
                price_no_discounts: price_no_discounts,
                contractor_price: contractor_price,
                employee_price: employee_price,
                tax_price: tax_price,
                manager_price: manager_price,
                profit_price: profit_price
            }
        }
    },
    getParams: function () {
        return this._saved_params;
    }
});