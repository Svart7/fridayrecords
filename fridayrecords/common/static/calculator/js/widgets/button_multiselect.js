var MULTISELECT_TEMPLATE_SELECTOR = '#multiselect_template';

WIDGETS.init_widget('ButtonMultiselect', {
    "init": function () {
        var widget = this,
            is_single_select = widget.$element.hasClass('single_select');

        widget._sub_header = widget.$element.find('.sub-header').html();
        widget.build_html();

        this.$element.find('.scroll_panel').each(function () {
            var $scroll_panel = $(this),
                scroll_widget = WIDGETS['ScrollPanel']($scroll_panel, {
                    min_viewport_width: true
                });

            if ($scroll_panel.hasClass('target')) {
                widget.target_scroller_widget = scroll_widget;
            } else {
                widget.source_scroller_widget = scroll_widget;
            }
        });

        widget.$element.on('click', '.source a', function () {
            var $a = $(this),
                id = $a.data('id');

            if (is_single_select && widget.getValue().indexOf(id) != -1) {
                return false;
            }

            if (widget.options.onChanging) {
                var ids = widget.target_scroller_widget.item_ids();
                ids.push(id);

                widget.options.onChanging(widget, ids.join(';'));
            }

            $a.clone(true).appendTo(widget.target_scroller_widget.$zone);
            widget.target_scroller_widget.resetWidth();

            if (widget.options.onChange) {
                widget.options.onChange(widget, widget.getValue());
            }

            return false;
        });

        widget.$element.on('click', '.target a', function () {
            var $a = $(this),
                id = $a.data('id');

            if (widget.options.onChanging) {
                var ids = widget.target_scroller_widget.item_ids(),
                    index = ids.indexOf(id);

                if (index != -1) {
                    ids.splice(index, 1)
                }

                widget.options.onChanging(widget, ids.join(';'));
            }

            $a.remove();
            widget.target_scroller_widget.resetWidth();

            if (widget.options.onChange) {
                widget.options.onChange(widget, widget.getValue());
            }

            return false;
        });
    },

    "template": function (context) {
        if (this._template) {
            return this._template(context);
        }

        this._template = $(MULTISELECT_TEMPLATE_SELECTOR).data('widget').template;
        return this._template(context);
    },

    "getValue": function () {
        return this.target_scroller_widget.item_ids().join(';');
    },

    "setValue": function (value) {
        var widget = this,
            value_array = value.split(';');

        widget.target_scroller_widget.$zone.html('');

        value_array.forEach(function (item) {
            var id = item.toString();

            var $source_block = widget.source_scroller_widget.$zone.find('[data-id="'+id+'"]');
            $source_block.clone(true).appendTo(widget.target_scroller_widget.$zone);
        });

        widget.target_scroller_widget.resetWidth();
    },

    "build_html": function (titles) {
        if ('titles' in this.options) {
            var titles = this.options.titles(this);
            if (titles) {
                var html = this.template({
                    titles: titles,
                    sub_header: this._sub_header
                });

                this.$element.html(html);
            }
        }
    }
});