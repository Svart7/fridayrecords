var GLOBAL_WIDGETS = [
        ['.js_csv_data', 'JSCSVData' ],
        ['.js_template_data', 'JSTemplateData'],
        ['.modal', 'Modal']
    ],
    PER_PANEL_WIDGETS = [
        ['.button_select', 'ButtonSelect'],
        ['.button_multiselect', 'ButtonMultiselect'],
        ['.slider', 'Slider'],
        ['.show_more', 'ShowMore'],
        ['.checkbox', 'Checkbox'],
        ['.price_panel_fixed', 'FixedPanel'],
        ['.price_counter', 'PriceCounter'],
        ['.price', 'Price'],
        ['.tooltip', 'Tooltip'],
        ['select', 'Select'],
        ['.input_integer', 'InputInteger'],
        ['textarea', 'Textarea'],
        ['iframe', 'Iframe']
    ],
    SELF_SELECTOR = '#calculator',
    SCROLLTO_SETTINGS = {
        duration: 400,
        offset: {left: 0, top: -115}
    };


WIDGETS.init_widget('Layout', {
    _on_panel_open: { },
    _on_panel_inited: { },
    _on_panel_close: { },
    _on_service_open: function() { },

    'set_on_panel_open': function (selector, callback) {
        this._on_panel_open[selector] = callback;
    },
    'set_on_panel_inited': function (selector, callback) {
        this._on_panel_inited[selector] = callback;
    },
    'set_on_panel_close': function (selector, callback) {
        this._on_panel_close[selector] = callback;
    },
    'set_on_service_open': function (callback) {
        this._on_service_open = callback;
    },

    _on_window_scroll: [],
    _service_id: null,
    'init': function () {
        var widget = this;

        if (!widget.$element.is(SELF_SELECTOR)) {
            console.error('please set layout to ' + SELF_SELECTOR);

            return;
        }

        widget._opened_panels[SELF_SELECTOR] = widget.$element;

        widget._init_widgets(SELF_SELECTOR, GLOBAL_WIDGETS);
        $(window).on('scroll', function (event) {
            widget._window_scroll(event);
        });
    },
    'add_on_scroll': function (callback) {
        this._on_window_scroll.push(callback);
    },
    'remove_on_scroll': function (callback) {
        var index = this._on_window_scroll.indexOf(callback);
        if (index > -1) {
            this._on_window_scroll.splice(index, 1);
        }
    },
    '_window_scroll': function (event) {
        $.each(this._on_window_scroll, function () { this(event); });
    },

    '_service_panels_selectors': [],
    'open_service': function(service_id) {
        var widget = this,
            service_ids = service_id.split(';');

        if (this._service_id == service_id) {
            return;
        }

        var panels = [],
            selectors = [],
            skip_service_id = false,
            $first_panel;

        $.each(service_ids, function (i) {
            var sub_service_id = this.toString(),
                j = 1;

            skip_service_id = (i == 0 && service_ids.length > 1);

            while (true) {
                var selector = '.step' + j + "-" + sub_service_id,
                    $panel_element = widget.$element.find(selector),
                    has_i_step = $panel_element.length;

                if (!has_i_step) {
                    break;
                }

                if (!$first_panel && !skip_service_id) {
                    $first_panel = $panel_element;
                }

                selectors.push(selector);
                j++;
            }
        });

        if (selectors.length) {
            $.merge(selectors, ['.price_panel']);

            $.each(widget._service_panels_selectors, function () {
                var selector = this.toString();
                if (selectors.indexOf(selector) == -1) {
                    widget.hide_panel(selector);
                }
            });

            $.each(selectors, function () {
                var selector = this.toString(),
                    $panel = widget._opened_panels[selector];

                if (!$panel) {
                    $panel = widget.open_panel(selector);
                }

                panels.push($panel.get(0));
            });

            widget._service_panels_selectors = selectors;
            widget._on_service_open(service_id, $(panels));

            if ($first_panel) {
                $(window).scrollTo($first_panel, SCROLLTO_SETTINGS);
            }
        } else {
            widget.close_service_panels();
            $(window).scrollTo('.step2', SCROLLTO_SETTINGS);
        }

        this._service_id = service_id;
    },
    'close_service_panels': function () {
        var widget = this;

        $.each(this._service_panels_selectors, function () {
            var selector = this.toString();
            widget.hide_panel(selector);
        });

        this._service_id = null;
    },

    // панель выше которой не будет подниматься цена
    '_opened_panels': {},
    '_price_panel_min_selector': null,
    'price_panel_min': function () {
        if (this._price_panel_min_selector) {
            if (this._price_panel_min_selector in this._opened_panels) {
                return this._opened_panels[this._price_panel_min_selector];
            }
        }

        return null;
    },
    
    'goto_panel': function (selector) {
        var $panel = this.open_panel(selector);
        $(window).scrollTo($panel, SCROLLTO_SETTINGS);
    },

    'open_panel': function(selector) {
        var $panel = this.$element.find(selector);
        this._opened_panels[selector] = $panel;

        $panel.show();
        this._init_widgets(selector, PER_PANEL_WIDGETS);

        if ($panel.is('.price_panel_min')) {
            this._price_panel_min_selector = selector;
        }

        if (selector in this._on_panel_open) {
            this._on_panel_open[selector]($panel);
        }

        return $panel;
    },
    'hide_panel': function (selector) {
        if (!(selector in this._opened_panels)) {
            return;
        }

        var $panel = this._opened_panels[selector];
        $panel.hide();

        if (selector in this._on_panel_close) {
            this._on_panel_close[selector]($panel);
        }

        this._service_panels_selectors = this._service_panels_selectors.filter(function (s) { return s != selector });
        delete this._opened_panels[selector];

        return $panel;
    },

    '_init_widgets': function (selector, config) {
        var $parent = this._opened_panels[selector];
        if ($parent.data('layout-inited')) {
            return;
        }

        $.each(config, function () {
            var selector = this[0],
                widget_name = this[1];

            $parent.find(selector).each(function () {
                WIDGETS[widget_name]($(this));
            });
        });

        $parent.data('layout-inited', true);

        if (selector in this._on_panel_inited) {
            this._on_panel_inited[selector]($parent);
        }
    }
});