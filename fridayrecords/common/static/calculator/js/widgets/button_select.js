var ARANGEMENT_TYPE_TEMPLATE_SELECTOR = '#arangement_type';

WIDGETS.init_widget('ButtonSelect', {
    "template": function (context) {
        if (this._template) {
            return this._template(context);
        }

        if (this.$element.is('.arangement_type')) {
            this._template = $(ARANGEMENT_TYPE_TEMPLATE_SELECTOR).data('widget').template;
            return this._template(context);
        }
        else
        {
            this._use_inner_template();
            return this.template(context);
        }
    },

    "init": function () {
        var widget = this;

        widget.$element.on('click', 'a', function () {
            var $a = $(this);
            widget.select_option($a);

            if ('onClick' in widget.options) {
                widget.options['onClick'](widget, $a);
            }
            return false;
        });

        widget._selected = null;
        widget.build();
    },

    "deselect": function () {
        var widget = this;

        if (widget._selected) {
            widget.$element.find("a.selected").removeClass('selected');
        }

        widget._selected = null;
    },
    "select_option": function ($button) {
        var widget = this;

        if (this.scroll_panel) {
            this.scroll_panel.set_visible({
                'left': $button.position().left,
                'width': $button.outerWidth() + 5
            });
        }

        var new_id = $button.data('id');
        if (!widget._selected || widget._selected != new_id) {
            if ('onChanging' in widget.options) {
                widget.options['onChanging'](widget, new_id);
            }

            widget.deselect();
            $button.addClass('selected');

            widget._selected = new_id;

            if ('onChange' in widget.options) {
                widget.options['onChange'](widget, new_id);
            }
        }

        return false;
    },
    "getValue": function () {
        return this._selected;
    },
    "setValue": function (value) {
        if (value === null) {
            this.deselect();
        } else {
            var $a = this.$element.find('[data-id="' + value + '"]');
            this.select_option($a);
        }
    },
    "build_html": function (titles) {
        var selected = this.getValue();

        var html = this.template(titles);
        this.$element.html(html);

        if (selected) {
            var $selected = this.$element.find('[data-id="' + selected + '"]');
            if ($selected.length) {
                $selected.addClass('selected');
            } else {
                this._selected = null;
            }
        }

        this.$element.find('.tooltip').each(function () {
            WIDGETS['Tooltip']($(this));
        });
    },
    "build": function () {
        if ('titles' in this.options) {
            var titles = this.options.titles(this);
            if (titles) {
                this.build_html(titles);
            }
        } else {
            this.build_html();
        }

        var $scroll_panel = this.$element.find('.scroll_panel');
        if ($scroll_panel.length && !$scroll_panel.data('widget')) {
            this.scroll_panel = WIDGETS['ScrollPanel']($scroll_panel);
        }
    }
});
