function float_parse (value) {
    var parsed_value = parseFloat(value.replace(/\s/g,"").replace(",", "."));
    if (isNaN(parsed_value)) {
        parsed_value = 0;
    }

    return parsed_value;
};

WIDGETS.init_widget('JSCSVData', {
    "init": function () {
        var widget = this,
            data = widget.$element.html();

        this.data = Papa.parse(data, {
            "header": true
        });
    }
});

WIDGETS.init_widget('JSTemplateData', {
    "init": function () {
        var widget = this,
            data = widget.$element.html();

        this.template = Handlebars.compile(data);
    }
});

WIDGETS.init_widget('Price', {
    "init": function () {
        this.value = 0;
    },
    "setValue": function (price) {
        var value = parseInt(price),
            value_txt = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&nbsp;");

        this.value = value;
        this.$element.html(value_txt);
    },
    "getValue": function () {
        return this.value;
    }
});

WIDGETS.init_widget('Iframe', {
    "init": function () {
        this.$element.attr('src', this.$element.data('src'));
    }
});

WIDGETS.init_widget('ShowMore', {
    _hidden: true,
    "init": function () {
        var widget = this,
            $open = widget.$element.find('.open'),
            $hidden_panel = widget.$element.find('.hidden_panel');

        $open.click(function () {
            if (widget._hidden) {
                $hidden_panel.show();
            } else {
                $hidden_panel.hide();
            }

            widget._hidden = !widget._hidden;

            return false;
        })
    }
});