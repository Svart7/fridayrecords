var URGENCY_SLIDER_TEMPLATE_SELECTOR = '#urgency_template';

WIDGETS.init_widget('Slider', {
    "template": function (context) {
        if (this._template) {
            return this._template(context);
        }

        if (this.$element.is('.urgency')) {
            this._template = $(URGENCY_SLIDER_TEMPLATE_SELECTOR).data('widget').template;
        } else {
            this._use_inner_template();
        }

        return this._template(context);
    },

    "build_titles": function () {
        var widget = this;

        if (widget.options.titles) {
            var titles = widget.options.titles(),
                html = widget.template(titles);

            widget.$element.html(html);
            widget.slider = null;
        }

        var $panel = widget.$element.find('.slider_panel'),
            $titles = $panel.find('.title'),
            titles_count = $titles.length,
            position_percent = 0,
            perstep_percent = 100/(titles_count-1);

        widget.titles = [];

        $titles.each(function (i) {
            var $title = $(this);

            if (i == titles_count-1) {
                $title.addClass('last');
            } else {
                $title.css('left', position_percent + "%");

                if (i == 0) {
                    $title.addClass('first');
                }

                position_percent += perstep_percent;
            }

            var value = $title.data('value');
            if (!value) {
                value = $title.text();
            }

            if ($title.attr('title')) {
                WIDGETS['Tooltip']($title, { classes: "small" });
            }

            widget.titles.push({
                "title": $title.text(),
                "value": value
            });
        });

        widget.span.max = widget.span.min + titles_count - 1;

        var slider_options = {
            min: widget.span.min,
            max: widget.span.max,
            slide: function (event, ui) {
                widget.onChanging(ui.value);
            }
        };

        widget.$panel = $panel;

        if (widget.slider) {
            $.each(Object.keys(slider_options), function () {
                var key = this.toString();
                widget.slider.slider('option', key, slider_options[key]);
            });
        } else {
            widget.slider = $panel.slider(slider_options);
        }
    },

    "init": function () {
        var widget = this;

        widget.$integer_input = null;
        widget.span = {
            min: 1,
            max: 1
        };

        var $input = widget.$element.find('input');
        if ($input.length) {
            var input_min = parseInt($input.attr('min'));
            widget.$integer_input = $input;

            widget.span.min = input_min;

            $input.on('change keyup mousewheel', function () {
                var value = parseInt($input.val());
                if (isNaN(value)) {
                    return;
                }

                if (value < input_min) {
                    value = input_min;
                }

                widget.slider.slider("value", value);
                widget.onChanging(value);
            });

            $input.val(input_min);
        }

        widget.build_titles();

        if ('onInit' in widget.options) {
            widget.options['onInit'](widget);
        }
    },
    "onChanging": function (new_value) {
        var widget = this,
            new_value_formatted = new_value;

        if (widget.$integer_input) {
            widget.$integer_input.val(new_value);
        } else {
            new_value_formatted = widget.titles[new_value-1].value
        }

        if ('onChanging' in widget.options) {
            widget.options.onChanging(widget, new_value_formatted);
        }
    },
    "getValue": function () {
        if (this.titles.length == 0) {
            return "";
        }

        var value = this.slider.slider("value");
        if (!this.$integer_input) {
            value = this.titles[value-1].value
        }

        return value;
    },
    "setValue": function (value) {
        if (this.titles.length == 0) {
            return;
        }

        var index = 0;
        this.titles.some(function (title, i) {
            if (title.value == value) {
                index = i;
                return true;
            }

            return false;
        });

        var inner_value = index + this.span.min;

        this.slider.slider("value", inner_value);
        if (this.$integer_input) {
            this.$integer_input.val(inner_value);
        }

        return value;
    }
});