WIDGETS.init_widget('FixedPanel', {
    init: function () {
        var widget = this;

        widget.$placeholder = widget.$element.find('.placeholder');
        widget.$floater = widget.$element.find('.floater');
        widget.$window = $(window);
    },
    init_height: function () {
        var widget = this;

        if (widget._height) {
            return;
        }

        widget._height = widget.$floater.outerHeight();
        widget.$placeholder.height(widget._height);
        this.$element.addClass('fixed');

        widget.check_bounds();
    },

    min_bound: 0,
    _panel_status: 'hiding',

    check_bounds: function () {
        if (!this._height) {
            return;
        }

        var scrollTop = this.$window.scrollTop() + this.$window.height() - this._height;
        if (scrollTop < this.min_bound) {
            var delta_h = this.min_bound - scrollTop;
            if (delta_h >= this._height) {
                if (this._panel_status == 'hidden') {
                    return;
                }

                this._panel_status = 'hidden';
                delta_h = this._height;
            } else {
                this._panel_status = 'hiding';
            }

            this.$floater.css('bottom', -1 * delta_h);
        } else if (scrollTop > this.$placeholder.offset().top) {
            if (this._panel_status == 'static') {
                return;
            }

            this._panel_status = 'static';
            this.$floater.css('bottom', 0);
            this.$element.removeClass('fixed').addClass('static');
        } else {
            if (this._panel_status == 'fixed') {
                return;
            }

            this._panel_status = 'fixed';
            this.$floater.css('bottom', 0);
            this.$element.removeClass('static').addClass('fixed');
        }
    }
});