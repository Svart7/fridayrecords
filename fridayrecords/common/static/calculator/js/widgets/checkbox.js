var CHECKBOX_TEMPLATE_SELECTOR = '#checkbox_template';

WIDGETS.init_widget('Checkbox', {
    selected: false,
    parent_hidden: false,

    "template": function (context) {
        if (this._template) {
            return this._template(context);
        }

        this._template = $(CHECKBOX_TEMPLATE_SELECTOR).data('widget').template;
        return this._template(context);
    },
    "render": function () {
        var widget = this;

        if (widget.selected) {
            widget.$element.addClass('selected');
        } else {
            widget.$element.removeClass('selected');
        }
    },
    "init": function () {
        var widget = this,
            html = this.template();

        widget.$element.append(html);

        if (widget.$element.hasClass('selected')) {
            widget.selected = true;
        }

        this.$element.click(function () {
            var new_value = !widget.selected;

            widget.onChanging(new_value);
            widget.selected = new_value;

            if ('onClick' in widget.options) {
                widget.options['onClick'](widget);
            }

            if ('rebuildLayout' in widget.options) {
                widget.options['rebuildLayout'](widget);
            }

            widget.render();

            return false;
        });

        if (widget.$element.hasClass('parent_hidden')) {
            widget.hideParent();
        }
    },

    "hideParent": function () {
        if (this.selected) {
            this.onChanging(false);
        }

        this.parent_hidden = true;
        this.$element.parent().hide();
    },
    "showParent": function () {
        if (this.selected) {
            this.onChanging(true);
        }

        this.parent_hidden = false;
        this.$element.parent().show();
    },
    "toggleParent": function () {
        if (this.parent_hidden) {
            this.showParent();
        } else {
            this.hideParent()
        }
    },

    "onChanging": function (new_value) {
        var widget = this;

        if ('onChanging' in widget.options) {
            widget.options.onChanging(widget, new_value);
        }
    },
    "getValue": function () {
        if (this.parent_hidden) {
            return false;
        }

        return this.selected;
    },
    "setValue": function (value) {
        this.selected = value;
        this.render();
    }
});