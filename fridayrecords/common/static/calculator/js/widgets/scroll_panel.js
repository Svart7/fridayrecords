WIDGETS.init_widget('ScrollPanel', {
    "resetWidth": function () {
        var widget = this,
            $zone = widget.$zone,
            $panel_children = $zone.children();

        widget._viewport_width = widget.$viewport.width();
        $zone.width(30000);

        var new_width = widget._viewport_width;

        if ($panel_children.length) {
            var $last_child = $($panel_children.get($panel_children.length - 1));
            new_width = $last_child.position().left + $last_child.outerWidth() + 6;

            widget.$element.removeClass('empty');
        } else {
            widget.$element.addClass('empty');
        }

        if (widget.options["min_viewport_width"] && new_width < widget._viewport_width) {
            new_width = this._viewport_width;
        }

        $zone.width(new_width);
        widget._scrollable_width = new_width;

        widget.check_buttons();

        if (widget._viewport_width < new_width) {
            var dragger_width = widget._viewport_width * widget._viewport_width / widget._scrollable_width;
            widget._dragger_width = dragger_width;
            widget.$scroller.show();
            widget.$scroll_dragger.width(dragger_width);
        } else {
            widget.$scroller.hide();
        }
    },
    "init": function () {
        var widget = this,
            $zone = widget.$element.find('.scroll_zone'),
            $viewport = widget.$element.find('.viewport');

        widget.$viewport = $viewport;
        widget._viewport_width = widget.$viewport.width();

        widget.$zone = $zone;

        widget.$left_arrow = widget.$element.find('.left_arrow');
        widget.$right_arrow = widget.$element.find('.right_arrow');
        widget.$scroller = widget.$element.find('.scroller');
        widget.$scroll_dragger = widget.$scroller.find('.dragger');

        widget.resetWidth();

        if (widget._scrollable_width < widget._viewport_width && !widget.options["min_viewport_width"]) {
            $zone.css('margin', '0 auto');

            return;
        }

        var $arrows = widget.$element.find('.arrow');
        $arrows.show();

        $viewport.on('scroll', function () {
            widget.check_buttons();
            widget.dragger_position(widget.$viewport.scrollLeft());
        });

        widget.$scroller.click(function (event) {
            var click_x = event.offsetX,
                desirable_dragger_x = click_x - (widget._dragger_width / 2),
                max_dragger_x = widget._viewport_width - widget._dragger_width;

            if (desirable_dragger_x < 0) {
                desirable_dragger_x = 0;
            } else if (desirable_dragger_x > max_dragger_x) {
                desirable_dragger_x = max_dragger_x;
            }

            var scroll_to = widget.dragger_x_to_scroll_left(desirable_dragger_x);
            widget.scroll_to(scroll_to);
        });

        var process_scroll_arrow = function ($element, ammount) {
            var _scrolling = false,
                _scroll = function () {
                    if (_scrolling) {
                        widget.scroll_by(ammount, function () {
                            if (_scrolling) {
                                _scroll();
                            }
                        });
                    } else {
                        $element.stop();
                    }
                };

            $element.on('mousedown', function () {
                _scrolling = true;
                _scroll();
            });

            $element.on('mouseup mouseleave', function () {
                _scrolling = false;
            });
        };

        process_scroll_arrow(widget.$left_arrow, -10);
        process_scroll_arrow(widget.$right_arrow, 10);

        widget.$scroll_dragger.draggable({
            "containment": "parent",
            "axis": "x",
            "drag": function () {
                var dragger_x = widget.$scroll_dragger.position().left,
                    scroll_to = widget.dragger_x_to_scroll_left(dragger_x);

                widget.$viewport.scrollLeft(scroll_to);
            },
            "start": function () {
                widget._disable_dragger_tracking = true;
            },
            "stop": function () {
                widget._disable_dragger_tracking = false;
            }
        });
    },
    item_ids: function () {
        var ids = [];
        this.$element.find('.button').each(function () {
            ids.push($(this).data('id'));
        });
        return ids;
    },
    "dragger_x_to_scroll_left": function (dragger_x) {
        var widget = this,
            kx = dragger_x / (widget._viewport_width - widget._dragger_width),
            scroll_to = kx * (widget._scrollable_width - widget._viewport_width);

        return scroll_to;
    },
    "set_visible": function (coordinates) {
        var widget = this,
            scrollLeft = this.$viewport.scrollLeft();

        if (coordinates.left < scrollLeft) {
            widget.scroll_to(coordinates.left);
        } else if (coordinates.left + coordinates.width > scrollLeft + widget._viewport_width) {
            widget.scroll_to(coordinates.left + coordinates.width - widget._viewport_width);
        }
    },
    "scroll_to": function(to, callback) {
        var widget = this;

        widget.$viewport.animate({"scrollLeft": to}, 20, function () {
            if (callback) {
                callback();
            }
        });
    },
    "scroll_by": function (ammount, callback) {
        var position = this.$viewport.scrollLeft() + ammount;
        this.scroll_to(position, callback);
    },
    "dragger_position": function (panel_offset_left) {
        var widget = this;
        if (widget._disable_dragger_tracking) {
            return;
        }

        var kx = panel_offset_left / (widget._scrollable_width - widget._viewport_width),
            dragger_x = kx * (widget._viewport_width - widget._dragger_width);

        widget.$scroll_dragger.css({"left": dragger_x});
    },
    "check_buttons": function () {
        var widget = this,
            position = widget.$zone.position(),
            set_disable_arrow = function (arrow_name, disable) {
                var key = "_"+arrow_name+"_disabled";
                if (!widget[key] && disable) {
                    widget[key] = true;
                    widget["$"+arrow_name].addClass('disable');
                } else if (widget[key] && !disable) {
                    widget[key] = false;
                    widget["$"+arrow_name].removeClass('disable');
                }
            };

        set_disable_arrow('left_arrow', position.left >= 0);
        set_disable_arrow('right_arrow', position.left + widget._scrollable_width - widget._viewport_width <= 0)
    }
});