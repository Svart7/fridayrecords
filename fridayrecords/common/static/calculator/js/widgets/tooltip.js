WIDGETS.init_widget('Tooltip', {
    "init": function () {
        if (/Mobi/.test(navigator.userAgent)) {
            return;
        }

        var classes = "ui-corner-all ui-widget-shadow";
        if (this.options['classes']) {
            classes += " " + this.options['classes'];
        }

        this.$element.tooltip({
            "position": {
                my: 'center top',
                at: 'center bottom+10',
                collision: 'none'
            },
            "hide": false,
            "show": {
                "delay": 500
            },
            classes: {
              "ui-tooltip": classes
            },
            content: function() {
                return $(this).attr('title');
            }
        });
    }
});