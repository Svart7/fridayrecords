# -*- coding: utf-8 -*-
import json
import pprint

from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string

from crm.models import Order
from fridayrecords import settings


def calculator_view(request):
    return render(request, 'calculator/base.html')


def calculator_order(request):
    if request.method == "POST":
        params = request.POST['params']

        order_params = json.loads(params)

        order = Order(name=order_params['order_name'],
                      telephone=order_params['order_phone'],
                      data=order_params)

        if 'order_address' in order_params:
            order.address = order_params['order_address']

        if order_params['order_email']:
            order.email = order_params['order_email']

        order.save()

        order_params['order_id'] = order.id
        order_params['order_added'] = order.added

        title = u'Заказ №%s %s' % (order.id, order_params['service'])

        message = render_to_string('calculator/mail/manager.txt', {'params': order_params})
        html_message = render_to_string('calculator/mail/manager.html', {'params': order_params})

        send_mail(title, message, settings.MAIN_EMAIl, [settings.MAIN_EMAIl], fail_silently=True, html_message=html_message)

        if order.email:
            message = render_to_string('calculator/mail/client.txt', {'params': order_params})
            html_message = render_to_string('calculator/mail/client.html', {'params': order_params})

            send_mail(title, message, settings.MAIN_EMAIl, [order.email], fail_silently=True, html_message=html_message)

        return JsonResponse({"result": "success", 'order_id': order.id, })

    return JsonResponse({"error": "unknown method, use POST"})
