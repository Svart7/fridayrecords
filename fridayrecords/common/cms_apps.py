# -*- coding: utf-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class CalculatorApp(CMSApp):
    name = u"Калькулятор услуг"
    urls = ["common.calc_urls"]

apphook_pool.register(CalculatorApp)
