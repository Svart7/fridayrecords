from __future__ import unicode_literals

from cms.extensions import TitleExtension, extension_pool
from django.db import models


class KeywordsExtension(TitleExtension):
    meta_keywords = models.TextField("Keywords", max_length=155, blank=True, null=True)

extension_pool.register(KeywordsExtension)
