from django.contrib import admin
from pricelist.models import PriceList

class PriceListAdmin(admin.ModelAdmin):
    list_display = ('group', 'name', 'price', )
    list_editable = ('price', )

admin.site.register(PriceList, PriceListAdmin)