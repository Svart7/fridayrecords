# -*- coding: utf-8 -*-
from django.db import models


class PriceList(models.Model):
    group = models.CharField("Группа", blank=True, max_length=200)
    name = models.CharField("Услуга", blank=True, max_length=200)
    price = models.IntegerField("Цена", blank=True)

    class Meta:
        ordering = ['group', 'name', ]

        verbose_name = u"Прайс-лист"
        verbose_name_plural = u"Прайс-лист"

    def __unicode__(self):
        return "%s %s" % (self.group, self.name)
