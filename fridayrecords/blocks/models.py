# -*- coding: utf-8 -*-
import datetime
import re

from django.db import models
from django.utils import timezone
from cms.models import CMSPlugin
from filer.fields.image import FilerImageField
from djangocms_text_ckeditor.fields import HTMLField
from cms.models.fields import PageField
from pricelist.models import PriceList


class SalesBlockPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200)
    content = HTMLField("Текст", max_length=2000, blank=True)

    price = models.CharField('Цена', max_length=200)

    price_special = models.CharField('Цена со спецпредложением', max_length=200, blank=True)
    date_to = models.DateTimeField("Окончание акции", blank=True, null=True)

    def is_special_offer(self):
        return self.date_to and self.date_to > timezone.now()

    def __unicode__(self):
        return self.header

    class Meta:
        verbose_name = u"Блок с спецпредложением"
        verbose_name_plural = u"Блоки с спецпредложениями"


class VideoBlockCategoryPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200)

    def __unicode__(self):
        return self.header

    class Meta:
        verbose_name = u"Блоки с видео"
        verbose_name_plural = u"Блоки с видео"


class VideoBlockPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200)
    content = HTMLField("Текст", max_length=2000, blank=True)
    video_html = models.TextField("Embed код видео", max_length=1000)

    def __unicode__(self):
        return self.header

    class Meta:
        verbose_name = u"Блок с видео"
        verbose_name_plural = u"Блоки с видео"


class OfferBlockCategoryPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200)

    def __unicode__(self):
        return self.header

    class Meta:
        verbose_name = u"Блоки с предложениями"
        verbose_name_plural = u"Блоки с предложениями"


class SoundcloudBlockPlugin(CMSPlugin):
    data = models.TextField("Код подключения soundcloud", max_length=1000)

    def __unicode__(self):
        match = re.search("playlists/\d+", self.data)
        if match:
            return match.group()
        else:
            return "cannot found name"

    class Meta:
        verbose_name = u"Блок soundcloud"
        verbose_name_plural = u"Блоки soundcloud"


class OfferBlockPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200)
    priceitem = models.ForeignKey(PriceList, blank=True, null=True)
    unit = models.CharField("Единица измерения", max_length=20, blank=True)
    content = models.TextField("Текст", max_length=2000, blank=True)
    gift = models.TextField("Подарок", max_length=200, blank=True)
    page = PageField(null=True, blank=True, help_text="Переход по клику на подробнее")

    def __unicode__(self):
        return self.header

    class Meta:
        verbose_name = u"Блок с предложением"
        verbose_name_plural = u"Блоки с предложением"


class IconBlockPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200)

    def __unicode__(self):
        return self.header

    def copy_relations(self, oldinstance):
        for block in oldinstance.iconblock_set.all():
            block.pk = None
            block.plugin = self
            block.save()

    class Meta:
        verbose_name = u"Блоки с иконками"
        verbose_name_plural = u"Блоки с иконками"


class IconBlock(models.Model):
    header = models.CharField("Заголовок", max_length=200)
    text = models.TextField("Текст", max_length=2000, blank=True, null=True)
    image = FilerImageField(related_name="image_active")
    image_passive = FilerImageField(blank=True, null=True, related_name="image_passive")
    sorter = models.FloatField("Сортировщик", default=0)
    plugin = models.ForeignKey(IconBlockPlugin)
    link = models.CharField("Ссылка", max_length=200, blank=True, null=True)

    def __unicode__(self):
        return self.header

    class Meta:
        verbose_name = u"Блок с иконкой"
        verbose_name_plural = u"Блоки с иконкой"
        ordering = ['sorter', ]


class HTMLBlockPlugin(CMSPlugin):
    MAIN_MODE = 1
    TARGET_MODE = 2
    ARTICLE_MODE = 3

    MODE_CHOISES = ((MAIN_MODE, "Главная",), (TARGET_MODE, "Целевая группа",), (ARTICLE_MODE, "Список статей"))

    header = models.CharField("Заголовок", blank=True, max_length=200)
    mode = models.IntegerField("Режим", choices=MODE_CHOISES, default=MAIN_MODE)
    content = HTMLField(blank=True)
    link = PageField(null=True, blank=True, help_text="Переход к статье")

    def __unicode__(self):
        return u"%s" % self.header

    class Meta:
        ordering = ['header', ]

        verbose_name = u"HTML блок"
        verbose_name_plural = u"HTML блок"