# -*- coding: utf-8 -*-

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib import admin
from blocks.models import IconBlockPlugin, IconBlock, OfferBlockPlugin, OfferBlockCategoryPlugin, \
    VideoBlockCategoryPlugin, VideoBlockPlugin, SalesBlockPlugin, \
    HTMLBlockPlugin, SoundcloudBlockPlugin


class SalesBlockPluginBase(CMSPluginBase):
    model = SalesBlockPlugin
    name = u"Блок с спецпредложением"
    render_template = "blocks/salesblock/salesblock.html"
    cache = False


class VideoBlockPluginBase(CMSPluginBase):
    model = VideoBlockPlugin
    name = u"Блок с видео"
    render_template = "blocks/videoblock.html"


class VideoBlockCategoryPluginBase(CMSPluginBase):
    model = VideoBlockCategoryPlugin
    name = u"Блок с кучей видео"
    render_template = "blocks/videoblockcategory.html"
    allow_children = True
    child_classes = ['VideoBlockPluginBase', ]


class OfferBlockPluginBase(CMSPluginBase):
    model = OfferBlockPlugin
    name = u"Блок с предложениeм"
    render_template = "blocks/offerblock.html"


class OfferBlockCategoryPluginBase(CMSPluginBase):
    model = OfferBlockCategoryPlugin
    name = u"Блок с предложениями"
    render_template = "blocks/offerblockcategory.html"
    allow_children = True
    child_classes = ['OfferBlockPluginBase', ]


class HTMLBlockPluginBase(CMSPluginBase):
    model = HTMLBlockPlugin
    name = u"Блок с html"
    render_template = "blocks/htmlblock.html"


class SoundcloudBlockPluginBase(CMSPluginBase):
    model = SoundcloudBlockPlugin
    name = u"Блок с soundcloud"
    render_template = "blocks/soundcloud.html"

    def render(self, context, instance, placeholder):
        context['instance'] = instance
        context['placeholder'] = placeholder
        if 'soundcloud_widgeted' in context:
            context['data'] = instance.data.replace('src="', 'data-src="')
        else:
            context['data'] = instance.data
        return context


class IconBlockInline(admin.StackedInline):
    model = IconBlock


class IconBlockPluginBase(CMSPluginBase):
    model = IconBlockPlugin
    name = u"Блок с иконками"
    render_template = "blocks/iconblock.html"
    inlines = [IconBlockInline, ]


plugin_pool.register_plugin(VideoBlockPluginBase)
plugin_pool.register_plugin(VideoBlockCategoryPluginBase)
plugin_pool.register_plugin(OfferBlockPluginBase)
plugin_pool.register_plugin(IconBlockPluginBase)
plugin_pool.register_plugin(OfferBlockCategoryPluginBase)
plugin_pool.register_plugin(SalesBlockPluginBase)
plugin_pool.register_plugin(HTMLBlockPluginBase)
plugin_pool.register_plugin(SoundcloudBlockPluginBase)
