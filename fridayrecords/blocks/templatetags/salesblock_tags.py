import locale
import calendar
import re
from django import template
from django.template.loader import render_to_string
from django.utils.encoding import force_unicode
import math
import time

register = template.Library()

def intdot(value):
    """
    Converts an integer to a string containing commas every three digits.
    For example, 3000 becomes '3.000' and 45000 becomes '45.000'.
    """
    orig = force_unicode(value)
    new = re.sub("^(-?\d+)(\d{3})", '\g<1>.\g<2>', orig)
    if orig == new:
        return new
    else:
        return intdot(new)

@register.filter
def add_separator(value):
    if value:
        return format(int(value),',d').replace(","," ")

@register.filter
def price_beauty(value):

    parts = value.split(' ')
    if len(parts) < 2:
        return value

    price = intdot(parts[0])
    price_parts = price.split('.')
    new_price_parts = []
    for i, part in enumerate(price_parts):
        tag_start = "<span class='part_%d'>%s" % (len(price_parts) - i, part)
        if i < len(price_parts) - 1:
            tag_start += "."

        new_price_parts.append("%s</span>" % tag_start)
    price = ''.join(new_price_parts)

    parts[0] = price
    return ' '.join(parts)

@register.simple_tag(takes_context=True)
def event_counter(context, salesblock):
    if not salesblock.date_to:
        return ""

    time_delta = calendar.timegm(salesblock.date_to.timetuple()) - time.time()
    if time_delta < 0:
        time_delta = 0

    remainings = []

    for span in [24*60*60, 60*60, 60, 1]:
        val = math.floor(time_delta / span)
        time_delta -= val * span
        remainings.append("%02d" % val)

    remainings = [list(x) for x in remainings]

    return render_to_string('blocks/salesblock/timer.html', {
        'remainings': remainings,
        'instance': salesblock,
    }, context_instance=context)
