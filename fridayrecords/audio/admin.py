from django.contrib import admin
from audio.models import Audio


class AudioAdmin(admin.ModelAdmin):
    fields = ('header', 'audio_file', )

    def get_model_perms(self, request):
        return {}

admin.site.register(Audio, AudioAdmin)