# -*- coding: utf-8 -*-

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib import admin
from audio.models import AudioCategoryPlugin, Audio, AudioPlugin


class AudioInline(admin.TabularInline):
    model = Audio


class AudioCategoryPluginBase(CMSPluginBase):
    model = AudioCategoryPlugin
    name = u"Категория блока с аудио"
    render_template = "audio_category.html"
    allow_children = True
    child_classes = ['AudioPluginBase', ]


class AudioPluginBase(CMSPluginBase):
    render_template = "audio_plugin.html"
    name = u"Блок с аудио"
    model = AudioPlugin
    inlines = [AudioInline, ]

    def render(self, context, instance, placeholder):
        context = super(AudioPluginBase, self).render(context, instance, placeholder)

        category = instance.parent
        audio_category = AudioCategoryPlugin.objects.filter(id=category.id)
        if len(audio_category) == 1 and audio_category[0].mode == AudioCategoryPlugin.SOUND_MODE:
            self.render_template = "audio_plugin_sound_mode.html"

        return context


plugin_pool.register_plugin(AudioCategoryPluginBase)
plugin_pool.register_plugin(AudioPluginBase)
