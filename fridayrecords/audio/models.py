# -*- coding: utf-8 -*-
from django.db import models
from cms.models import CMSPlugin
from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField


class AudioCategoryPlugin(CMSPlugin):
    CUSTOM_MODE = 1
    SOUND_MODE = 2

    MODE_CHOISES = ((CUSTOM_MODE, "Обычный",), (SOUND_MODE, "Звуки студии", ))

    header = models.CharField("Заголовок", max_length=200, blank=True)
    mode = models.IntegerField("Режим", choices=MODE_CHOISES, default=CUSTOM_MODE)

    def __unicode__(self):
        return u"%s" % self.header


class AudioPlugin(CMSPlugin):
    header = models.CharField("Заголовок", max_length=200, blank=True)
    image = FilerImageField(blank=True, null=True, help_text="Для картинки с оборудованием в режиме \"Звуки студии\"")

    def __unicode__(self):
        return u"%s" % self.header

    def copy_relations(self, oldinstance):
        for audio in oldinstance.audio_set.all():
            audio.pk = None
            audio.plugin = self
            audio.save()


class Audio(models.Model):
    header = models.CharField("Заголовок", max_length=200)
    audio_file = FilerFileField()
    plugin = models.ForeignKey(AudioPlugin)

    class Meta:
        verbose_name = "Аудио"
        verbose_name_plural = "Аудио"

    def __unicode__(self):
        return u"%s" % self.header

